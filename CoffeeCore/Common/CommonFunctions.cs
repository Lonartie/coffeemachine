﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeCore
{
    public static class CommonFunctions
    {
        public static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static string ToString<T>(List<T> list)
        {
            string result = "(";
            for (int n = 0; n < list.Count; n++)
                result += list[n].ToString() + (n != list.Count - 1 ? "|" : ")");
            return result;
        }
    }
}
