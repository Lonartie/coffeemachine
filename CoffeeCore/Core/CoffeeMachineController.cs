﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

namespace CoffeeCore
{
    public class CoffeeMachineController
    {
        //----------------------------------------------------
        // public members and functions
        //----------------------------------------------------

        public static void SaveStorage(CoffeeMachineStorage storage, string configPath)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(CoffeeMachineStorage));
                using (StreamWriter writer = new StreamWriter(configPath))
                    serializer.Serialize(writer, storage);
                Logging.Emit("The configuration file has been saved to: " + configPath, LogCase.Info);
            }
            catch (Exception e)
            {
                Logging.Emit("The configuration file could not be saved!", LogCase.Error);
            }
        }

        public static CoffeeMachineStorage LoadFromString(string res)
        {
            CoffeeMachineStorage obj = null;
            XmlSerializer serializer = new XmlSerializer(typeof(CoffeeMachineStorage));
            using (StringReader reader = new StringReader(res))
                obj = (CoffeeMachineStorage)serializer.Deserialize(reader);
            return obj;
        }

        public void SaveConfig() =>
            SaveStorage(storage, storage.ConfigPath);

        public CoffeeMachineController(string configPath, bool createServer = true)
        {
            Logging.Init();
            Logging.Emit("Booting coffee machine", LogCase.Info);
            if (createServer)
                StartServer();
            initializeMembers(configPath);
            storage.ConfigPath = configPath;
        }

        public Resources GetNeededResources()
        {
            Logging.Emit("Calculating needed resources", LogCase.Info);
            if (storage.SelectedOffer == null)
                return new Resources();
            return Resources.Calculate(storage.SelectedOffer.resources, storage.SelectedExtras.Select(x => x.resources).ToList());
        }


        public bool CanProceed() =>
            (storage.AvailableResources - GetNeededResources()) > storage.ThresholdFuel;

        public List<string> GetAvailableOfferNames()
        {
            Logging.Emit("Requesting available offers", LogCase.Info);
            return storage.AvailableOffers.Select(x => x.name).ToList();
        }

        public List<string> GetAvailableExtrasNames()
        {
            Logging.Emit("Requesting available extras", LogCase.Info);
            return storage.AvailableExtras.Select(x => x.name).ToList();
        }

        public Resources GetAvailableResources()
        {
            Logging.Emit("Requesting available resources", LogCase.Info);
            return storage.AvailableResources;
        }

        public void Storno()
        {
            storage.SelectedExtras.Clear();
            storage.SelectedOffer = null;
        }

        public Operation GetCurrentOperation()
        {
            Logging.Emit("Requesting current operation", LogCase.Info);
            return storage.CurrentOperation;
        }

        public CoffeeMachineController SelectProductByName(string ProductName)
        {
            storage.SelectedExtras.Clear();
            storage.SelectedOffer = null;
            Logging.Emit("Selecting product: " + ProductName, LogCase.Info);
            storage.SelectedOffer = storage.AvailableOffers.FirstOrDefault(x => x.name == ProductName);
            return this;
        }

        public CoffeeMachineController AddExtraByName(string ExtraName)
        {
            Logging.Emit("Adding extra: " + ExtraName, LogCase.Info);
            var extra = storage.AvailableExtras.FirstOrDefault(x => x.name == ExtraName);
            if (extra.resources.IsMultiplier)
                ResetSize();
            storage.SelectedExtras.Add(extra);
            return this;
        }

        public float GetPriceOfSelectedProduct()
        {
            Logging.Emit("Requesting price of selected product", LogCase.Info);
            return GetNeededResources().money * (-1);
        }

        public bool IsEnoughMoney(List<float> inputCoins)
        {
            Logging.Emit("Checking for enough money: [costs:" + GetPriceOfSelectedProduct() + "] [input:" + CommonFunctions.ToString(inputCoins) + "]", LogCase.Info);
            return inputCoins.Sum() >= GetPriceOfSelectedProduct();
        }

        public List<float> InsertMoneyForSelectedProductAndGetChange(List<float> inputCoins)
        {
            Logging.Emit("changing money: [costs:" + GetPriceOfSelectedProduct() + "] [input:" + CommonFunctions.ToString(inputCoins) + "]", LogCase.Info);
            if (!IsEnoughMoney(inputCoins))
            {
                Logging.Emit("Not enough money!", LogCase.Warning);
                return new List<float>();
            }

            foreach (var coin in inputCoins)
                if (Math.Abs(((int)(coin * 100.0)) / 100.0f - coin) > 0.01)
                {
                    Logging.Emit("input coin [" + coin + "] does not exists in this country!", LogCase.Error);
                    return new List<float>();
                }

            var sum = inputCoins.Sum();
            int changeSum = (int)Math.Round((sum - GetPriceOfSelectedProduct()) * 100);
            Logging.Emit("change sum = " + (changeSum / 100f), LogCase.Info);
            var change = new List<float>();

            while (changeSum > 10E-6)
            {
                int tempCoin;
                if (changeSum >= 5000) tempCoin = 5000;
                else if (changeSum >= 2000) tempCoin = 2000;
                else if (changeSum >= 1000) tempCoin = 1000;
                else if (changeSum >= 500) tempCoin = 500;
                else if (changeSum >= 200) tempCoin = 200;
                else if (changeSum >= 100) tempCoin = 100;
                else if (changeSum >= 050) tempCoin = 050;
                else if (changeSum >= 020) tempCoin = 020;
                else if (changeSum >= 010) tempCoin = 010;
                else if (changeSum >= 005) tempCoin = 005;
                else tempCoin = 001;

                changeSum -= tempCoin;
                change.Add(tempCoin / 100f);
            }

            Logging.Emit("change is: " + CommonFunctions.ToString(change), LogCase.Info);

            return change;
        }

        public CoffeeMachineController CalculateOffer()
        {
            Logging.Emit("Calculating offer! Available resources before: " + storage.AvailableResources.ToString(), LogCase.Info);
            var neededResources = GetNeededResources();
            Logging.Emit("needed resources for product: " + neededResources.ToString(), LogCase.Info);
            storage.AvailableResources -= neededResources;
            Logging.Emit("Available resources after: " + storage.AvailableResources.ToString(), LogCase.Info);
            Logging.Emit("saving new values to config!", LogCase.Info);

            SaveStats();
            storage.SelectedExtras.Clear();
            storage.SelectedOffer = null;
            SaveStorage(storage, storage.ConfigPath);

            return this;
        }

        public void StopServer()
        {
            if (server != null)
            {
                Logging.Emit("Stopping server!", LogCase.Warning);
                server.StopServer();
            }
        }

        public ref CoffeeMachineStorage GetStorageObject()
        {
            Logging.Emit("Requesting storage object", LogCase.Warning);
            return ref storage;
        }

        public string GetSelectedProductName() =>
            storage.SelectedOffer.name;

        public void ResetSize() =>
            Logging.Try(() =>
            storage.SelectedExtras.Remove(storage.SelectedExtras.First(x => x.resources.IsMultiplier))
                , "trying to delete extra which is not selected! [size]");

        public void DeleteExtra(string extraName) =>
            Logging.Try(() =>
                    storage.SelectedExtras.Remove(storage.SelectedExtras.First(x => x.name == extraName))
                , "trying to delete extra which is not selected! " + extraName);

        public void SetPrice(string name, float price) => 
            Logging.Try(() =>
            {
                storage.AvailableOffers[
                        storage.AvailableOffers.IndexOf(storage.AvailableOffers.First(x => x.name == name))]
                    .price = price;
                storage.AvailableOffers[
                        storage.AvailableOffers.IndexOf(storage.AvailableOffers.First(x => x.name == name))].resources
                    .money = -price;


            }, "this product does not exist: " + name);

        public void StartServer()
        {
            server = new Server();
            server.OnCommandReceived += ParseRemoteCommand;
            server.StartServer();
        }

        //----------------------------------------------------
        // private members and functions
        //----------------------------------------------------

        private void initializeMembers(string configPath)
        {
            var done = Logging.Try(() =>
            {
                XmlSerializer serializer = new XmlSerializer(typeof(CoffeeMachineStorage));
                using (StreamReader reader = new StreamReader(configPath))
                    storage = (CoffeeMachineStorage)serializer.Deserialize(reader);
            }, "The configuration file could not be loaded! Creating new one!", LogCase.Warning);

            if (!done)
            {
                storage = new CoffeeMachineStorage
                {
                    CurrentOperation = Operation.SelectProduct,
                    AvailableResources = new Resources
                    {
                        sugar = 1500,
                        milk = 1500,
                        coffee = 1500,
                        espresso = 1500,
                        money = 0,
                    },
                    NormalResources = new Resources
                    {
                        sugar = 1500,
                        milk = 1500,
                        coffee = 1500,
                        espresso = 1500,
                        money = 100,
                    },
                    ThresholdFuel = new Resources
                    {
                        sugar = 50,
                        milk = 50,
                        coffee = 50,
                        espresso = 50,
                        money = 100,
                    },
                    AvailableOffers = InitOffers(),
                    AvailableExtras = InitExtras(),
                    SelectedExtras = new List<Offer>(),
                    SelectedOffer = null,
                    statistics = new Statistics(),
                };
            }

            SaveStorage(storage, configPath);
        }

        private void SaveStats()
        {
            if (storage.SelectedOffer.name == "Kaffee") storage.coffee++;
            if (storage.SelectedOffer.name == "Heiße Milch") storage.milk++;
            if (storage.SelectedOffer.name == "Heißes Wasser") storage.water++;
            if (storage.SelectedOffer.name == "Kakao") storage.cacao++;
            if (storage.SelectedOffer.name == "Espresso") storage.espresso++;
            if (storage.SelectedOffer.name == "Cappuccino") storage.cappuccino++;

            storage.statistics.Add("Available Coffee", storage.AvailableResources.coffee);
            storage.statistics.Add("Available Espresso", storage.AvailableResources.espresso);
            storage.statistics.Add("Available Milk", storage.AvailableResources.milk);
            storage.statistics.Add("Available Sugar", storage.AvailableResources.sugar);
            storage.statistics.Add("Available Money", storage.AvailableResources.money);

            storage.statistics.Add("Coffee", storage.coffee);
            storage.statistics.Add("Espresso", storage.espresso);
            storage.statistics.Add("Cappuccino", storage.cappuccino);
            storage.statistics.Add("Milk", storage.milk);
            storage.statistics.Add("Water", storage.water);
            storage.statistics.Add("Cacao", storage.cacao);
        }

        private void ParseRemoteCommand(string command)
        {
            string path = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            ProcessStartInfo info =
                new ProcessStartInfo(path + "/ProcessHelper.exe");

            switch (command)
            {
                case "GET_LOG":
                    server.SendCommand("|LOG|" + Logging.GetCompleteLog());
                    break;
                case "GET_RES":
                    server.SendCommand("|RES|" + GetResourcesAsString());
                    break;
                case "GET_STA":
                    server.SendCommand("|STA|" + Statistics.GetAsString());
                    break;
                case "DEL_CON":
                    Logging.Emit("Deleting config file: " + storage.ConfigPath, LogCase.Warning);
                    if (File.Exists(storage.ConfigPath))
                        File.Delete(storage.ConfigPath);
                    break;
                case "GET_VER":
                    try
                    {
                        StreamReader reader =
                            new StreamReader(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/version.txt");
                        server.SendCommand("|VER|" + reader.ReadToEnd().Split("\n"[0])[0]);
                        reader.Close();
                    }
                    catch (Exception) { }
                    break;
                case "RESTART":
                    Logging.Emit("Restarting machine", LogCase.Warning);
                    info.Arguments = "/r";
                    Process.Start(info);
                    break;
                case "SHUTDWN":
                    Logging.Emit("Shutting down machine", LogCase.Warning);
                    info.Arguments = "/s";
                    Process.Start(info);
                    break;
                case "UPDATE":
                    Logging.Emit("Updating machine", LogCase.Warning);
                    info.Arguments = "/u";
                    Process.Start(info);
                    break;
            }
            SetValues(command);
            Thread.Sleep(100);
        }

        private void SetValues(string command)
        {
            if (command.Contains("|SET_RES|"))
            {
                command = command.Remove(0, 9);
                Logging.Emit("Overriding Resources", LogCase.Warning);
            }
            else if (command.Contains("|SET_STD|"))
            {
                command = command.Remove(0, 9);
                Logging.Emit("Overriding Std values", LogCase.Warning);
            }
            else if (command.Contains("|SET_PPR|"))
            {
                command = command.Remove(0, 9);
                Logging.Emit("Overriding prices", LogCase.Warning);
            }
            else if (command.Contains("|PW|"))
            {
                command = command.Remove(0, 4);
                string pw = command;
                storage.password = pw;
                SaveConfig();
                server?.SendCommand("OK");
            }
        }
        
        private string GetResourcesAsString()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CoffeeMachineStorage));
            string content = "";
            using (MemoryStream stream = new MemoryStream())
            {
                serializer.Serialize(stream, storage);
                content = Encoding.UTF8.GetString(stream.ToArray());
            }
            return content;
        }

        private List<Offer> InitOffers()
        {
            return new List<Offer>
            {
                new Offer("Kaffee", 1.1f, new Resources {coffee = 20}),
                new Offer("Kakao", 1f, new Resources {milk = 50}),
                new Offer("Cappuccino", 1.4f, new Resources {coffee = 30}),
                new Offer("Espresso", 1.2f, new Resources {espresso = 20}),
                new Offer("Heißes Wasser", 0.5f),
                new Offer("Heiße Milch", 0.75f, new Resources {milk = 50}),
            };
        }

        private List<Offer> InitExtras()
        {
            return new List<Offer>
            {
                new Offer("Klein", 0.0f, new Resources(1f), OfferType.ExtraToProduct),
                new Offer("Mittel", 0.25f, new Resources(1.5f), OfferType.ExtraToProduct),
                new Offer("Groß", 0.5f, new Resources(2f), OfferType.ExtraToProduct),
                new Offer("Kanne", 1.5f, new Resources(3f), OfferType.ExtraToProduct),
                new Offer("Milch", 0.2f, new Resources(0, 0, 10, 0), OfferType.ExtraToProduct),
                new Offer("Zucker", 0.1f, new Resources(0, 0, 0, 10), OfferType.ExtraToProduct),
            };
        }

        private Server server = null;

        private CoffeeMachineStorage storage = null;
    }
}
