﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeCore
{
    public class CoffeeMachineStorage
    {
        //----------------------------------------------------
        // public members and functions
        //----------------------------------------------------

        public Resources AvailableResources;

        public Resources NormalResources;

        public List<Offer> AvailableOffers;

        public List<Offer> AvailableExtras;

        public List<Offer> SelectedExtras;

        public Offer SelectedOffer;

        public Operation CurrentOperation;

        public Resources ThresholdFuel;

        public string ConfigPath;

        public Statistics statistics = null;

        public string password = "8998";

        public int
            coffee = 0,
            espresso = 0,
            cacao = 0,
            water = 0,
            milk = 0,
            cappuccino = 0;
    }
}
