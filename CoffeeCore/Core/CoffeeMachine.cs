﻿#region Doxygen MainPage

/*! \authors Leon Gierschner <br> Domenic Henne <br> Erik Stößer <br> René Mumdey
 */

// created with: https://html-online.com/editor/

/*! \mainpage Coffee-Machine - Team 2 - Documentation
 * <p>&nbsp;</p>
 * <p>&nbsp;<strong><span style="text-decoration: underline;">Project-Description:</span></strong></p>
 * <ul>
 * <li><em>This software is an exemplary implementation of a consumer-friendly coffee machine software</em></li>
 * <li><em>Our goal is to write a consumer-friendly software implementation of a coffee machine</em></li>
 * </ul>
 * <p><span style="text-decoration: underline;"><strong>Project-Target:</strong></span></p>
 * <ul>
 * <li><em>The software is written in C# for easy usage<br /></em></li>
 * <li><em>It supports any version of windows that runs .NET 4.5 (even Vista)</em></li>
 * <li><em>It will run well on low power systems that have less &lt; 4GB RAM and &lt; 1 x 1.6 GHz </em></li>
 * </ul>
 * <a href="src/img.png"><img src="src/img.png" width="250"></img></a>
 * <a href="src/img2.png"><img src="src/img2.png" width="250"></img></a>
 * <a href="src/img3.png"><img src="src/img3.png" width="250"></img></a>
 * <a href="src/img4.png"><img src="src/img4.png" width="250"></img></a>
 * <a href="src/img5.png"><img src="src/img5.png" width="250"></img></a>
*/

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeCore
{
    public class CoffeeMachine
    {
        //----------------------------------------------------
        // public members and functions
        //----------------------------------------------------

        /// <summary>
        /// adds a listener to a specific log case
        /// </summary>
        /// <param name="logCase">the log case to listen to</param>
        /// <param name="function">the function to call when case is fired</param>
        public static void AddLoggingListener(LogCase logCase, Func<string, bool> function) =>
            Logging.AddListener(logCase, function);

        /// <summary>
        /// creates a new coffee machine with config file at specified location.
        /// creates on if none exists
        /// </summary>
        /// <param name="configPath">the path to the configuration file</param>
        /// <param name="createServer">whether or not to create the server in back-end</param>
        public CoffeeMachine(string configPath, bool createServer = true) =>
            controller = new CoffeeMachineController(configPath, createServer);

        ~CoffeeMachine() =>
            controller.StopServer();

        /// <summary>
        /// stops the server that is running in the background for maintenance
        /// </summary>
        public void StopServer() =>
            controller.StopServer();

        public void StartServer() =>
            controller.StartServer();

        /// <summary>
        /// Saves the configuration.
        /// </summary>
        public void SaveConfig() =>
            controller.SaveConfig();

        /// <summary>
        /// Gets the controller.
        /// </summary>
        /// <returns>the controller</returns>
        public CoffeeMachineController GetController() =>
            controller;

        /// <summary>
        /// Determines whether or not there are enough resources
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance can proceed; otherwise, <c>false</c>.
        /// </returns>
        public bool CanProceed() =>
            controller.
                CanProceed();

        /// <summary>
        /// Get the needed resources it takes to process the current order.
        /// </summary>
        /// <returns></returns>
        public Resources GetNeededResources() =>
            controller.GetNeededResources();

        /// <summary>
        /// Get the storage.
        /// </summary>
        /// <returns>the storage of this coffee machine</returns>
        public CoffeeMachineStorage GetStorage() =>
            controller.GetStorageObject();

        /// <summary>
        /// aborts the current order.
        /// </summary>
        public void Storno() =>
            controller.Storno();

        /// <summary>
        /// get the available resources such as sugar, milk, coffee beans etc.
        /// </summary>
        /// <returns>the available resources this coffee machine currently has</returns>
        public Resources GetAvailableResources() =>
            controller.GetAvailableResources();

        /// <summary>
        /// get the available AvailableOffers to show on UI
        /// </summary>
        /// <returns>the names of all available AvailableOffers</returns>
        public List<string> GetAvailableOfferNames() =>
            controller.GetAvailableOfferNames();

        /// <summary>
        /// get the available extras names to show on UI
        /// </summary>
        /// <returns>the available extras names e.g. Groß ;or; Klein ;or; Milch</returns>
        public List<string> GetAvailableExtrasNames() =>
            controller.GetAvailableExtrasNames();

        /// <summary>
        /// get the currently executed operation
        /// </summary>
        /// <returns>the current operation</returns>
        public Operation GetCurrentOperation() =>
            controller.GetCurrentOperation();

        /// <summary>
        /// set the selected product by its name
        /// </summary>
        /// <param name="ProductName">the product name e.g. Kaffee</param>
        public void SelectProduct(string ProductName) =>
            controller.SelectProductByName(ProductName);

        /// <summary>
        /// add an extra to the order
        /// </summary>
        /// <param name="ExtraName">the name of the extra offer e.g. Groß ;or; Sugar</param>
        public void AddExtra(string ExtraName) =>
            controller.AddExtraByName(ExtraName);

        /// <summary>
        /// Deletes the given extra.
        /// </summary>
        /// <param name="extraName">Name of the extra.</param>
        public void DeleteExtra(string extraName) =>
            controller.DeleteExtra(extraName);

        /// <summary>
        /// get the price of the selected product
        /// </summary>
        /// <returns>the price of the product in €</returns>
        public float GetPriceOfSelectedProduct() =>
            controller.GetPriceOfSelectedProduct();

        /// <summary>
        /// if you want to know if you put in enough money for the selected product
        /// </summary>
        /// <param name="inputCoins">the input coins in €</param>
        /// <returns>boolean true if money is enough, false otherwise</returns>
        public bool IsEnoughMoney(List<float> inputCoins) =>
            controller.IsEnoughMoney(inputCoins);

        /// <summary>
        /// calculates the change for a transaction
        /// </summary>
        /// <param name="inputCoins">the input coins in €</param>
        /// <returns>the output coins given on the price of the selected product</returns>
        public List<float> InsertMoneyForSelectedProductAndGetChange(List<float> inputCoins) =>
            controller.InsertMoneyForSelectedProductAndGetChange(inputCoins);

        /// <summary>
        /// rounds up the order by calculating the offer and producing the product
        /// </summary>
        public void CalculateOffer() =>
            controller.CalculateOffer();

        /// <summary>
        /// Gets the name of the selected product.
        /// </summary>
        /// <returns>the name</returns>
        public string GetSelectedProductName() =>
            controller.GetSelectedProductName();

        /// <summary>
        /// Resets the size of the selected product.
        /// </summary>
        public void ResetSize() =>
            controller.ResetSize();

        //----------------------------------------------------
        // private members and functions
        //----------------------------------------------------

        /// <summary>
        /// the controller object to run all the behaviour
        /// </summary>
        private CoffeeMachineController controller = null;
    }
}
