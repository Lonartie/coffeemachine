﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeCore
{
    public enum LogCase
    {
        Error,
        Warning,
        Info
    };

    public struct LogInfo
    {
        public LogCase logCase;
        public string message;
        public DateTime timeStamp;

        public override string ToString()
        {
            return timeStamp.ToLongDateString() + " - " + timeStamp.ToLongTimeString() + ": [" + logCase.ToString() + "] " + message;
        }
    };

    public static class Logging
    {
        /// <summary>
        /// adds a listener to receive logs of specific log case
        /// </summary>
        /// <param name="logCase">the log case to listen to</param>
        /// <param name="function">the function to call when case is fired</param>
        public static void AddListener(LogCase logCase, Func<string, bool> function)
        {
            if (!listeners.ContainsKey(logCase))
                listeners.Add(logCase, new List<Func<string, bool>>());
            listeners[logCase].Add(function);
        }

        public static bool? Emit(string message, LogCase logCase)
        {
            bool result = true;
            allLogs.Add(new LogInfo { logCase = logCase, message = message, timeStamp = DateTime.Now });
            if (listeners.ContainsKey(logCase))
                foreach (var listener in listeners[logCase])
                    result = result && (listener?.Invoke(message) == true);
            return result;
        }

        public static bool Try(Action action, string message, LogCase logCase = LogCase.Error)
        {
            try
            { action?.Invoke(); return true; }
            catch (Exception)
            { Emit(message, logCase); }

            return false;
        }

        public static string GetCompleteLog()
        {
            string logs = "";
            int size = allLogs.Count;
            LogInfo[] templog = new LogInfo[size];
            allLogs.CopyTo(templog);
            foreach (var log in templog)
                logs += log.ToString() + "\r\n";
            return logs;
        }

        public static void Init()
        {
            allLogs.Clear();
        }

        private static Dictionary<LogCase, List<Func<string, bool>>> listeners =
            new Dictionary<LogCase, List<Func<string, bool>>>();

        private static List<LogInfo> allLogs = new List<LogInfo>();
    }
}
