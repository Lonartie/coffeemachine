﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SimpleTCP;

namespace CoffeeCore
{
    public class Client
    {
        public delegate void CommandReceiver(string command);

        private string ip;

        private int port;

        private bool pong = false;

        private SimpleTcpClient client;

        public CommandReceiver OnCommandReceived;

        public Client(string ip = "localhost", int port = 9312)
        {
            this.ip = ip;
            this.port = port;
        }

        ~Client()
        {
            try
            {
                if (client.TcpClient.Connected) client.Disconnect();
            }
            catch (Exception) { }
        }

        public void ConnectToServer()
        {
            client = new SimpleTcpClient();
            client.DataReceived += (sender, message) =>
            {
                if (message.MessageString == "PONG%") pong = true;
                else
                {
                    foreach (var comm in message.MessageString.Split('%'))
                        if (!string.IsNullOrEmpty(comm))
                            OnCommandReceived?.Invoke(comm);
                }
            };
            client.Connect(ip, port);
        }

        public bool SendCommand(string command)
        {
            try
            {
                client.Write(command + "%");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DisconnectFromServer()
        {
            client.Disconnect();
        }

        public bool IsConnected()
        {
            if (!SendCommand("PING")) return false;
            var start = DateTime.Now;
            while (true)
            {
                if (pong)
                {
                    pong = false;
                    return true;
                }
                var now = DateTime.Now;
                if (now - start > new TimeSpan(0, 0, 1))
                    return false;
                Thread.Sleep(1);
            }
        }
    }
}
