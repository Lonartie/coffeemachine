﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Channels;
using System.Text;
using SimpleTCP;

namespace CoffeeCore
{
    public class Server
    { 
        public delegate void CommandReceiver(string command);

        public delegate void ClientConnected(string clientIP);

        private string ip;

        private int port;

        private SimpleTcpServer server;

        public CommandReceiver OnCommandReceived;

        public ClientConnected OnClientConnected;


        public Server(string ip = "0.0.0.0", int port = 9312)
        {
            this.ip = ip;
            this.port = port;
        }

        ~Server()
        {
            try
            {
                if (server.IsStarted) server.Stop();
            }
            catch (Exception) { }
        }

        public bool SendCommand(string command)
        {
            try
            {
                server.Broadcast(command + "%");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void StartServer()
        {
            server = new SimpleTcpServer();
            server.DataReceived += (sender, message) =>
            {
                if (message.MessageString == "PING%") SendCommand("PONG");
                else
                {
                    foreach (var comm in message.MessageString.Split('%'))
                        if (!string.IsNullOrEmpty(comm))
                            OnCommandReceived?.Invoke(comm);
                }
            };
            server.ClientConnected += (sender, e) => OnClientConnected?.Invoke(e.Client.LocalEndPoint.ToString());
            server.Start(IPAddress.Parse(ip), port);
        }

        public void StopServer()
        {
            try
            {
                server.Stop();
            }
            catch (Exception) { }
        }
    }
}
