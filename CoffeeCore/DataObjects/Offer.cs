﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeCore
{
    public enum OfferType
    {
        EndProduct,
        ExtraToProduct,
    };

    /// <summary>
    /// Offer defines an offer to use for the coffee machine
    /// it defines a name, a price, and how much resources it will take
    /// </summary>
    [System.Serializable]
    public class Offer
    {
        /// <summary>
        /// defines the name of the offer
        /// </summary>
        public string name;

        /// <summary>
        /// the type of the offer
        /// </summary>
        public OfferType offerType;

        /// <summary>
        /// defines the price of the offer
        /// </summary>
        public float price;

        /// <summary>
        /// defines an amount of resources this offer needs
        /// </summary>
        public Resources resources;

        /// <summary>
        /// std constructor initializing the members
        /// </summary>
        /// <param name="name">the name of the offer</param>
        /// <param name="price">the price of the offer</param>
        /// <param name="resources">the amount of resources this offer needs</param>
        public Offer(string name, float price, Resources resources = null, OfferType offerType = OfferType.EndProduct)
        {
            if (object.ReferenceEquals(resources, null))
                resources = new Resources();

            this.name       = name;
            this.price      = price;
            this.resources  = resources;
            this.offerType  = offerType;
            resources.money = -price;
        }

        public Offer() { }
    }
}
