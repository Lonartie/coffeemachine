﻿using System;
using System.Collections.Generic;

namespace CoffeeCore
{
    /// <summary>
    /// Resources defines an amount of resources
    /// </summary>
    public class Resources
    {
        /// <summary>
        /// sums up all resources in a list of resources
        /// </summary>
        /// <param name="resources">the list of resources to sum up</param>
        /// <returns>the sum of all resources</returns>
        public static Resources Sum(List<Resources> resources)
        {
            Resources sum = new Resources();
            foreach (var res in resources)
                if (!res.IsMultiplier)
                    sum += res;
            return sum;
        }

        public static Resources Calculate(Resources offer, List<Resources> extras)
        {
            Resources multiplier = null;
            foreach (var extra in extras)
                if (extra.IsMultiplier)
                {
                    multiplier = extra;
                    extras.Remove(extra);
                    break;
                }

            if (multiplier == null)
                return (offer + Sum(extras));

            return (offer * multiplier) + Sum(extras);
        }

        /// <summary>
        /// defines an amount of coffee beans in grams
        /// </summary>
        public float coffee;

        /// <summary>
        /// defines an amount of milk in ml
        /// </summary>
        public float milk;

        /// <summary>
        /// defines an amount of sugar in grams
        /// </summary>
        public float sugar;

        /// <summary>
        /// defines the amount of money in €
        /// </summary>
        public float money;

        /// <summary>
        /// defines the amount of espresso in grams
        /// </summary>
        public float espresso;

        public float multiplier;

        public bool IsMultiplier = false;

        /// <summary>
        /// std constructor initializing member variables
        /// </summary>
        /// <param name="coffee">the amount of coffee</param>
        /// <param name="espresso">the amount of espresso</param>
        /// <param name="milk">the amount of milk</param>
        /// <param name="sugar">the amount of sugar</param>
        /// <param name="money">the amount money</param>
        public Resources(float coffee, float espresso, float milk, float sugar)
        {
            this.coffee = coffee;
            this.milk = milk;
            this.espresso = espresso;
            this.sugar = sugar;
        }

        public Resources()
        {
        }

        public Resources(float multiplier)
        {
            this.multiplier = multiplier;
            //this.money = money;
            IsMultiplier = true;
        }

        public static Resources operator +(Resources a, Resources b) =>
            new Resources
            {
                money = a.money + b.money,
                sugar = a.sugar + b.sugar,
                milk = a.milk + b.milk,
                coffee = a.coffee + b.coffee,
                espresso = a.espresso + b.espresso
            };

        public static Resources operator -(Resources a, Resources b) =>
            new Resources
            {
                money = a.money - b.money,
                sugar = a.sugar - b.sugar,
                milk = a.milk - b.milk,
                coffee = a.coffee - b.coffee,
                espresso = a.espresso - b.espresso
            };

        public static Resources operator *(Resources a, Resources b) =>
            new Resources
            {
                coffee = a.coffee * b.multiplier,
                espresso = a.espresso * b.multiplier,
                milk = a.milk * b.multiplier,
                sugar = a.sugar * b.multiplier,
                money = a.money + b.money,
            };

        public static bool operator >(Resources a, Resources b) =>
            (
                a.coffee > b.coffee &&
                a.espresso > b.espresso &&
                a.milk > b.milk &&
                a.money < b.money &&
                a.sugar > b.sugar
            );

        public static bool operator <(Resources a, Resources b) =>
            (
                a.coffee < b.coffee &&
                a.espresso < b.espresso &&
                a.milk < b.milk &&
                a.money > b.money &&
                a.sugar < b.sugar
            );

        public static bool operator ==(Resources a, Resources b)
        {
            if ((object.ReferenceEquals(a, null) || object.ReferenceEquals(b, null)) &&
                (!object.ReferenceEquals(a, null) || !object.ReferenceEquals(b, null))) return false;
            else if (object.ReferenceEquals(a, null) && object.ReferenceEquals(b, null)) return true;
            else
                return a.ToString().Equals(b.ToString());
        }

        public static bool operator !=(Resources a, Resources b) =>
            !(a == b);

        public override string ToString() =>
            $"milk:{milk}, coffee:{coffee}, espresso:{espresso}, sugar:{sugar}, money:{money}";

        private static bool FloatCompare(float a, float b, int n = 2) =>
            (int)Math.Round(a * Math.Pow(10, n)) == (int)Math.Round(b * Math.Pow(10, n));
    }
}