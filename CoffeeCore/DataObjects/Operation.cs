﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeCore
{
    /// <summary>
    /// Operation defines a state which operation is currently executed
    /// </summary>
    public enum Operation
    {
        InsertMoney     = 0,
        SelectProduct   = 1,
        SelectExtras    = 2,
        ExecuteOrder    = 3,
        Reset           = 4,
    };
}
