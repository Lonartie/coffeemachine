﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CoffeeCore
{
    public class Statistics
    {
        public static Statistics Instance = null;

        public static string GetAsString()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Statistics));
            string content = "";
            using (MemoryStream stream = new MemoryStream())
            {
                serializer.Serialize(stream, Instance);
                content = Encoding.UTF8.GetString(stream.ToArray());
            }
            return content;
        }

        public static Statistics FromString(string str)
        {
            Statistics obj = null;
            XmlSerializer serializer = new XmlSerializer(typeof(Statistics));
            using (StringReader reader = new StringReader(str))
                obj = (Statistics)serializer.Deserialize(reader);
            return obj;
        }

        public Statistics()
        {
            Instance = this;
        }

        public List<string> names = new List<string>();

        public List<List<DateTime>> dates = new List<List<DateTime>>();

        public List<List<float>> values = new List<List<float>>();

        public void Add(string name, float value)
        {
            var date = DateTime.Now;

            if (!names.Contains(name))
            {
                names.Add(name);
                dates.Add(new List<DateTime>());
                values.Add(new List<float>());
            }

            int index = names.IndexOf(name);

            if (!dates[index].Contains(date))
            {
                dates[index].Add(date);
                values[index].Add(value);
                return;
            }

            int dindex = dates[index].IndexOf(date);
            values[index][dindex] = value;
        }
    }
}
