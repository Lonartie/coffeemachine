﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using AppDomain = System.AppDomain;

namespace ProcessHelper
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Update(-1);
                return;
            }

            if (args[0] == "/r") Restart();
            else if (args[0] == "/s") Shutdown();
            else if (args[0] == "/u") Update();
            else if (args[0] == "/u1") Update(1);
            else if (args[0] == "/u2") Update(2);
            else if (args[0] == "/start") Start();
            else if (args[0] == "/c") Clean();
            return;
        }

        static void Restart()
        {
            Shutdown();
            Start();
        }

        static void Start()
        {
            try
            {
                Process.Start(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/CoffeeMachineUI.exe");
            }catch (Exception){}
        }

        static void Shutdown()
        {
            foreach (var process in Process.GetProcessesByName("CoffeeMachineUI").ToList().TempAdd(Process.GetProcessesByName("CoffeeMachineTerminal").ToList()))
                process.Kill();
        }

        static void Clean()
        {
            Thread.Sleep(100);

            if (File.Exists(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/update.zip"))
                File.Delete(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/update.zip");

            if (File.Exists(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/ProcessHelperCopy.exe"))
                File.Delete(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/ProcessHelperCopy.exe");

            if (Directory.Exists(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/update"))
                Directory.Delete(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/update", true);
        }

        static void Update(int n = 0)
        {
            if (n == 1 || n == 2)
            {
                Thread.Sleep(100);
                Console.WriteLine("shutting down");
                Shutdown();

                var file = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/update.zip";
                var output = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/update";
                var thisDir = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;

                if (!Directory.Exists(output))
                    Directory.CreateDirectory(output);
                else
                    Directory.Delete(output, true);


                foreach (var oFile in new DirectoryInfo(thisDir).GetFiles())
                    if (oFile.Name != "ProcessHelperCopy.exe")
                    {
                        bool success = false;
                        while (!success)
                        {
                            try
                            {
                                oFile.Delete();
                                success = true;
                            } catch(Exception){}
                        }
                    }

                Uri uri = new Uri(
                    "http://lonartie.ddns.net:8080/job/Coffee-Machine%20(Windows)%20(x64)/lastBuild/artifact/out.zip");
                Console.WriteLine("Downloading Update!");
                using (var client = new WebClient())
                    Task.Run(() => client.DownloadFileTaskAsync(uri, file)).GetAwaiter().GetResult();

                Console.WriteLine("Unzipping update");
                ZipFile.ExtractToDirectory(file, output);

                Console.WriteLine("Copying updated files");
                foreach (var oFile in new DirectoryInfo(output).GetFiles())
                {
                    bool success = false;
                    while(!success)
                    {
                        try
                        {
                            oFile.CopyTo(thisDir + "/" + oFile.Name, true);
                            success = true;
                        }
                        catch (Exception){}
                    }
                }

                if(n ==1)
                    Start();

                try
                {
                    ProcessStartInfo info2 = new ProcessStartInfo(
                        AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/ProcessHelper.exe",
                        "/c");
                    Process.Start(info2);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            else
            {
                File.Copy(
                    AppDomain.CurrentDomain.SetupInformation.ApplicationBase + 
                    AppDomain.CurrentDomain.SetupInformation.ApplicationName,
                    AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/ProcessHelperCopy.exe", true);
                ProcessStartInfo info = new ProcessStartInfo(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/ProcessHelperCopy.exe",
                    n == 0 ? "/u1" : "/u2");
                Process.Start(info);
            }
        }
    }

    public static class Extensions
    {
        public static List<Process> TempAdd(this List<Process> a, List<Process> b)
        {
            List<Process> newList = new List<Process>();
            newList.AddRange(a);
            newList.AddRange(b);
            return newList;
        }
    }
}
