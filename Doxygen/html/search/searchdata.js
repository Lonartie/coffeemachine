var indexSectionsWithContent =
{
  0: "cdlmnoprs",
  1: "cloprs",
  2: "c",
  3: "dor",
  4: "cmnprs",
  5: "o",
  6: "n"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Pages"
};

