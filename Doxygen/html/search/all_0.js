var searchData=
[
  ['client',['Client',['../class_coffee_core_1_1_client.html',1,'CoffeeCore']]],
  ['coffee',['coffee',['../class_coffee_core_1_1_resources.html#ac95c6445e57ca283cf50c38b508c5371',1,'CoffeeCore::Resources']]],
  ['coffeecore',['CoffeeCore',['../namespace_coffee_core.html',1,'']]],
  ['coffeecore_5ftest',['CoffeeCore_Test',['../class_coffee_machine_1_1_test_1_1_coffee_core___test.html',1,'CoffeeMachine::Test']]],
  ['coffeemachine',['CoffeeMachine',['../class_coffee_core_1_1_coffee_machine.html',1,'CoffeeCore.CoffeeMachine'],['../namespace_coffee_machine.html',1,'CoffeeMachine']]],
  ['coffeemachinecontroller',['CoffeeMachineController',['../class_coffee_core_1_1_coffee_machine_controller.html',1,'CoffeeCore']]],
  ['coffeemachinestorage',['CoffeeMachineStorage',['../class_coffee_core_1_1_coffee_machine_storage.html',1,'CoffeeCore']]],
  ['coffeemachineui',['CoffeeMachineUI',['../namespace_coffee_machine_u_i.html',1,'']]],
  ['properties',['Properties',['../namespace_coffee_machine_u_i_1_1_properties.html',1,'CoffeeMachineUI']]],
  ['test',['Test',['../namespace_coffee_machine_1_1_test.html',1,'CoffeeMachine']]]
];
