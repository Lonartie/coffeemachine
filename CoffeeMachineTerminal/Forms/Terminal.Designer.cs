﻿namespace CoffeeMachineTerminal
{
    partial class Terminal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.connection = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.IPField = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.SystemControls = new System.Windows.Forms.TabPage();
            this.delBT = new System.Windows.Forms.Button();
            this.upBT = new System.Windows.Forms.Button();
            this.restBT = new System.Windows.Forms.Button();
            this.shutBT = new System.Windows.Forms.Button();
            this.Resources = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.espressoPer = new System.Windows.Forms.Label();
            this.EspressoBar = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.EspressoField = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.MoneyField = new System.Windows.Forms.TextBox();
            this.SugarField = new System.Windows.Forms.TextBox();
            this.MilkField = new System.Windows.Forms.TextBox();
            this.CoffeeField = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.moneyPer = new System.Windows.Forms.Label();
            this.sugarPer = new System.Windows.Forms.Label();
            this.milkPer = new System.Windows.Forms.Label();
            this.coffeePer = new System.Windows.Forms.Label();
            this.MoneyBar = new System.Windows.Forms.PictureBox();
            this.SugarBar = new System.Windows.Forms.PictureBox();
            this.MilkBar = new System.Windows.Forms.PictureBox();
            this.CoffeeBar = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Statistics = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chartBuyed = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartAvailable = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.RequestStats = new System.Windows.Forms.Button();
            this.Log = new System.Windows.Forms.TabPage();
            this.ReqLogBT = new System.Windows.Forms.Button();
            this.logTB = new System.Windows.Forms.RichTextBox();
            this.ConnectionStatus = new System.Windows.Forms.Label();
            this.ConnectionTransmitter = new System.Windows.Forms.Timer(this.components);
            this.setPW = new System.Windows.Forms.Button();
            this.password = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.SystemControls.SuspendLayout();
            this.Resources.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EspressoBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MoneyBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SugarBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MilkBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CoffeeBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Statistics.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartBuyed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartAvailable)).BeginInit();
            this.Log.SuspendLayout();
            this.SuspendLayout();
            // 
            // connection
            // 
            this.connection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.connection.Location = new System.Drawing.Point(814, 11);
            this.connection.Name = "connection";
            this.connection.Size = new System.Drawing.Size(75, 23);
            this.connection.TabIndex = 0;
            this.connection.Text = "Connect";
            this.connection.UseVisualStyleBackColor = true;
            this.connection.Click += new System.EventHandler(this.connection_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "IP-Address";
            // 
            // IPField
            // 
            this.IPField.Location = new System.Drawing.Point(78, 12);
            this.IPField.Name = "IPField";
            this.IPField.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.IPField.Size = new System.Drawing.Size(140, 20);
            this.IPField.TabIndex = 2;
            this.IPField.Text = "localhost";
            this.IPField.WordWrap = false;
            this.IPField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IPField_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(221, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = ":9312";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.SystemControls);
            this.tabControl1.Controls.Add(this.Resources);
            this.tabControl1.Controls.Add(this.Statistics);
            this.tabControl1.Controls.Add(this.Log);
            this.tabControl1.Location = new System.Drawing.Point(12, 38);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(882, 414);
            this.tabControl1.TabIndex = 4;
            // 
            // SystemControls
            // 
            this.SystemControls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.SystemControls.Controls.Add(this.password);
            this.SystemControls.Controls.Add(this.setPW);
            this.SystemControls.Controls.Add(this.delBT);
            this.SystemControls.Controls.Add(this.upBT);
            this.SystemControls.Controls.Add(this.restBT);
            this.SystemControls.Controls.Add(this.shutBT);
            this.SystemControls.Location = new System.Drawing.Point(4, 22);
            this.SystemControls.Name = "SystemControls";
            this.SystemControls.Size = new System.Drawing.Size(874, 388);
            this.SystemControls.TabIndex = 0;
            this.SystemControls.Text = "System";
            // 
            // delBT
            // 
            this.delBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delBT.Location = new System.Drawing.Point(153, 49);
            this.delBT.Name = "delBT";
            this.delBT.Size = new System.Drawing.Size(98, 23);
            this.delBT.TabIndex = 4;
            this.delBT.Text = "Delete Config";
            this.delBT.UseVisualStyleBackColor = true;
            this.delBT.Click += new System.EventHandler(this.DeleteConfig);
            // 
            // upBT
            // 
            this.upBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upBT.Location = new System.Drawing.Point(164, 122);
            this.upBT.Name = "upBT";
            this.upBT.Size = new System.Drawing.Size(75, 23);
            this.upBT.TabIndex = 3;
            this.upBT.Text = "Update";
            this.upBT.UseVisualStyleBackColor = true;
            this.upBT.Click += new System.EventHandler(this.UpdateRemote);
            // 
            // restBT
            // 
            this.restBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restBT.Location = new System.Drawing.Point(277, 49);
            this.restBT.Name = "restBT";
            this.restBT.Size = new System.Drawing.Size(75, 23);
            this.restBT.TabIndex = 2;
            this.restBT.Text = "Restart";
            this.restBT.UseVisualStyleBackColor = true;
            this.restBT.Click += new System.EventHandler(this.Restart);
            // 
            // shutBT
            // 
            this.shutBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shutBT.Location = new System.Drawing.Point(45, 49);
            this.shutBT.Name = "shutBT";
            this.shutBT.Size = new System.Drawing.Size(75, 23);
            this.shutBT.TabIndex = 0;
            this.shutBT.Text = "Shutdown";
            this.shutBT.UseVisualStyleBackColor = true;
            this.shutBT.Click += new System.EventHandler(this.Shutdown);
            // 
            // Resources
            // 
            this.Resources.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.Resources.Controls.Add(this.label4);
            this.Resources.Controls.Add(this.espressoPer);
            this.Resources.Controls.Add(this.EspressoBar);
            this.Resources.Controls.Add(this.pictureBox11);
            this.Resources.Controls.Add(this.EspressoField);
            this.Resources.Controls.Add(this.label3);
            this.Resources.Controls.Add(this.pictureBox9);
            this.Resources.Controls.Add(this.MoneyField);
            this.Resources.Controls.Add(this.SugarField);
            this.Resources.Controls.Add(this.MilkField);
            this.Resources.Controls.Add(this.CoffeeField);
            this.Resources.Controls.Add(this.label14);
            this.Resources.Controls.Add(this.pictureBox8);
            this.Resources.Controls.Add(this.label13);
            this.Resources.Controls.Add(this.pictureBox7);
            this.Resources.Controls.Add(this.label12);
            this.Resources.Controls.Add(this.pictureBox6);
            this.Resources.Controls.Add(this.label11);
            this.Resources.Controls.Add(this.pictureBox5);
            this.Resources.Controls.Add(this.label10);
            this.Resources.Controls.Add(this.label9);
            this.Resources.Controls.Add(this.label8);
            this.Resources.Controls.Add(this.label7);
            this.Resources.Controls.Add(this.moneyPer);
            this.Resources.Controls.Add(this.sugarPer);
            this.Resources.Controls.Add(this.milkPer);
            this.Resources.Controls.Add(this.coffeePer);
            this.Resources.Controls.Add(this.MoneyBar);
            this.Resources.Controls.Add(this.SugarBar);
            this.Resources.Controls.Add(this.MilkBar);
            this.Resources.Controls.Add(this.CoffeeBar);
            this.Resources.Controls.Add(this.pictureBox4);
            this.Resources.Controls.Add(this.pictureBox3);
            this.Resources.Controls.Add(this.pictureBox2);
            this.Resources.Controls.Add(this.pictureBox1);
            this.Resources.Location = new System.Drawing.Point(4, 22);
            this.Resources.Name = "Resources";
            this.Resources.Size = new System.Drawing.Size(874, 388);
            this.Resources.TabIndex = 1;
            this.Resources.Text = "Resources";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Esp.";
            // 
            // espressoPer
            // 
            this.espressoPer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.espressoPer.AutoSize = true;
            this.espressoPer.Location = new System.Drawing.Point(55, 165);
            this.espressoPer.Name = "espressoPer";
            this.espressoPer.Size = new System.Drawing.Size(30, 13);
            this.espressoPer.TabIndex = 32;
            this.espressoPer.Text = "50 %";
            // 
            // EspressoBar
            // 
            this.EspressoBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.EspressoBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.EspressoBar.Location = new System.Drawing.Point(58, 81);
            this.EspressoBar.Name = "EspressoBar";
            this.EspressoBar.Size = new System.Drawing.Size(19, 75);
            this.EspressoBar.TabIndex = 31;
            this.EspressoBar.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox11.BackColor = System.Drawing.Color.Gray;
            this.pictureBox11.Location = new System.Drawing.Point(58, 20);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(19, 135);
            this.pictureBox11.TabIndex = 30;
            this.pictureBox11.TabStop = false;
            // 
            // EspressoField
            // 
            this.EspressoField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EspressoField.Location = new System.Drawing.Point(397, 50);
            this.EspressoField.Name = "EspressoField";
            this.EspressoField.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.EspressoField.Size = new System.Drawing.Size(39, 20);
            this.EspressoField.TabIndex = 29;
            this.EspressoField.Text = "750";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(325, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "= Espresso =";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.pictureBox9.Location = new System.Drawing.Point(298, 47);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(20, 20);
            this.pictureBox9.TabIndex = 28;
            this.pictureBox9.TabStop = false;
            // 
            // MoneyField
            // 
            this.MoneyField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MoneyField.Location = new System.Drawing.Point(397, 129);
            this.MoneyField.Name = "MoneyField";
            this.MoneyField.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.MoneyField.Size = new System.Drawing.Size(39, 20);
            this.MoneyField.TabIndex = 26;
            this.MoneyField.Text = "50";
            // 
            // SugarField
            // 
            this.SugarField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SugarField.Location = new System.Drawing.Point(397, 103);
            this.SugarField.Name = "SugarField";
            this.SugarField.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SugarField.Size = new System.Drawing.Size(39, 20);
            this.SugarField.TabIndex = 25;
            this.SugarField.Text = "750";
            // 
            // MilkField
            // 
            this.MilkField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MilkField.Location = new System.Drawing.Point(397, 76);
            this.MilkField.Name = "MilkField";
            this.MilkField.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.MilkField.Size = new System.Drawing.Size(39, 20);
            this.MilkField.TabIndex = 24;
            this.MilkField.Text = "750";
            // 
            // CoffeeField
            // 
            this.CoffeeField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CoffeeField.Location = new System.Drawing.Point(397, 23);
            this.CoffeeField.Name = "CoffeeField";
            this.CoffeeField.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CoffeeField.Size = new System.Drawing.Size(39, 20);
            this.CoffeeField.TabIndex = 23;
            this.CoffeeField.Text = "750";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(325, 131);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "= Euro = ";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.pictureBox8.Location = new System.Drawing.Point(298, 126);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(20, 20);
            this.pictureBox8.TabIndex = 21;
            this.pictureBox8.TabStop = false;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(325, 105);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 18;
            this.label13.Text = "= Sugar = ";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pictureBox7.Location = new System.Drawing.Point(298, 100);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(20, 20);
            this.pictureBox7.TabIndex = 19;
            this.pictureBox7.TabStop = false;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(325, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "= Milk = ";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox6.BackColor = System.Drawing.Color.Honeydew;
            this.pictureBox6.Location = new System.Drawing.Point(298, 73);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(20, 20);
            this.pictureBox6.TabIndex = 17;
            this.pictureBox6.TabStop = false;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(325, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "= Coffee = ";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox5.BackColor = System.Drawing.Color.Chocolate;
            this.pictureBox5.Location = new System.Drawing.Point(298, 20);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(20, 20);
            this.pictureBox5.TabIndex = 15;
            this.pictureBox5.TabStop = false;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(166, 178);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Euro";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(91, 178);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Sugar";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(128, 178);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Milk";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Coffee";
            // 
            // moneyPer
            // 
            this.moneyPer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.moneyPer.AutoSize = true;
            this.moneyPer.Location = new System.Drawing.Point(166, 165);
            this.moneyPer.Name = "moneyPer";
            this.moneyPer.Size = new System.Drawing.Size(30, 13);
            this.moneyPer.TabIndex = 10;
            this.moneyPer.Text = "50 %";
            // 
            // sugarPer
            // 
            this.sugarPer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.sugarPer.AutoSize = true;
            this.sugarPer.Location = new System.Drawing.Point(91, 165);
            this.sugarPer.Name = "sugarPer";
            this.sugarPer.Size = new System.Drawing.Size(30, 13);
            this.sugarPer.TabIndex = 9;
            this.sugarPer.Text = "50 %";
            // 
            // milkPer
            // 
            this.milkPer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.milkPer.AutoSize = true;
            this.milkPer.Location = new System.Drawing.Point(128, 165);
            this.milkPer.Name = "milkPer";
            this.milkPer.Size = new System.Drawing.Size(30, 13);
            this.milkPer.TabIndex = 8;
            this.milkPer.Text = "50 %";
            // 
            // coffeePer
            // 
            this.coffeePer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.coffeePer.AutoSize = true;
            this.coffeePer.Location = new System.Drawing.Point(17, 165);
            this.coffeePer.Name = "coffeePer";
            this.coffeePer.Size = new System.Drawing.Size(30, 13);
            this.coffeePer.TabIndex = 5;
            this.coffeePer.Text = "50 %";
            // 
            // MoneyBar
            // 
            this.MoneyBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.MoneyBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.MoneyBar.Location = new System.Drawing.Point(169, 81);
            this.MoneyBar.Name = "MoneyBar";
            this.MoneyBar.Size = new System.Drawing.Size(19, 75);
            this.MoneyBar.TabIndex = 7;
            this.MoneyBar.TabStop = false;
            // 
            // SugarBar
            // 
            this.SugarBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.SugarBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.SugarBar.Location = new System.Drawing.Point(93, 81);
            this.SugarBar.Name = "SugarBar";
            this.SugarBar.Size = new System.Drawing.Size(19, 75);
            this.SugarBar.TabIndex = 6;
            this.SugarBar.TabStop = false;
            // 
            // MilkBar
            // 
            this.MilkBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.MilkBar.BackColor = System.Drawing.Color.Honeydew;
            this.MilkBar.Location = new System.Drawing.Point(131, 81);
            this.MilkBar.Name = "MilkBar";
            this.MilkBar.Size = new System.Drawing.Size(19, 75);
            this.MilkBar.TabIndex = 5;
            this.MilkBar.TabStop = false;
            // 
            // CoffeeBar
            // 
            this.CoffeeBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.CoffeeBar.BackColor = System.Drawing.Color.Chocolate;
            this.CoffeeBar.Location = new System.Drawing.Point(20, 114);
            this.CoffeeBar.Name = "CoffeeBar";
            this.CoffeeBar.Size = new System.Drawing.Size(19, 42);
            this.CoffeeBar.TabIndex = 4;
            this.CoffeeBar.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox4.BackColor = System.Drawing.Color.Gray;
            this.pictureBox4.Location = new System.Drawing.Point(169, 20);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(19, 135);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox3.BackColor = System.Drawing.Color.Gray;
            this.pictureBox3.Location = new System.Drawing.Point(93, 20);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(19, 135);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox2.BackColor = System.Drawing.Color.Gray;
            this.pictureBox2.Location = new System.Drawing.Point(131, 20);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(19, 135);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.BackColor = System.Drawing.Color.Gray;
            this.pictureBox1.Location = new System.Drawing.Point(20, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(19, 135);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Statistics
            // 
            this.Statistics.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.Statistics.Controls.Add(this.tableLayoutPanel1);
            this.Statistics.Controls.Add(this.RequestStats);
            this.Statistics.Location = new System.Drawing.Point(4, 22);
            this.Statistics.Name = "Statistics";
            this.Statistics.Size = new System.Drawing.Size(874, 388);
            this.Statistics.TabIndex = 2;
            this.Statistics.Text = "Statistics";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.chartBuyed, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.chartAvailable, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(869, 355);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // chartBuyed
            // 
            this.chartBuyed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.chartBuyed.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartBuyed.Legends.Add(legend1);
            this.chartBuyed.Location = new System.Drawing.Point(436, 2);
            this.chartBuyed.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chartBuyed.Name = "chartBuyed";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartBuyed.Series.Add(series1);
            this.chartBuyed.Size = new System.Drawing.Size(431, 351);
            this.chartBuyed.TabIndex = 1;
            this.chartBuyed.Text = "chart2";
            // 
            // chartAvailable
            // 
            this.chartAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            chartArea3.Name = "money";
            this.chartAvailable.ChartAreas.Add(chartArea2);
            this.chartAvailable.ChartAreas.Add(chartArea3);
            legend2.Name = "Legend1";
            this.chartAvailable.Legends.Add(legend2);
            this.chartAvailable.Location = new System.Drawing.Point(2, 2);
            this.chartAvailable.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chartAvailable.Name = "chartAvailable";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chartAvailable.Series.Add(series2);
            this.chartAvailable.Size = new System.Drawing.Size(430, 351);
            this.chartAvailable.TabIndex = 0;
            // 
            // RequestStats
            // 
            this.RequestStats.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RequestStats.Location = new System.Drawing.Point(767, 362);
            this.RequestStats.Name = "RequestStats";
            this.RequestStats.Size = new System.Drawing.Size(104, 23);
            this.RequestStats.TabIndex = 7;
            this.RequestStats.Text = "Request Stats";
            this.RequestStats.UseVisualStyleBackColor = true;
            this.RequestStats.Click += new System.EventHandler(this.RequestStats_Click);
            // 
            // Log
            // 
            this.Log.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.Log.Controls.Add(this.ReqLogBT);
            this.Log.Controls.Add(this.logTB);
            this.Log.Location = new System.Drawing.Point(4, 22);
            this.Log.Name = "Log";
            this.Log.Size = new System.Drawing.Size(874, 388);
            this.Log.TabIndex = 3;
            this.Log.Text = "System-Log";
            // 
            // ReqLogBT
            // 
            this.ReqLogBT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ReqLogBT.Location = new System.Drawing.Point(767, 362);
            this.ReqLogBT.Name = "ReqLogBT";
            this.ReqLogBT.Size = new System.Drawing.Size(104, 23);
            this.ReqLogBT.TabIndex = 6;
            this.ReqLogBT.Text = "Request Log";
            this.ReqLogBT.UseVisualStyleBackColor = true;
            this.ReqLogBT.Click += new System.EventHandler(this.ReqLogBT_Click);
            // 
            // logTB
            // 
            this.logTB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logTB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.logTB.Location = new System.Drawing.Point(4, 3);
            this.logTB.Name = "logTB";
            this.logTB.ReadOnly = true;
            this.logTB.Size = new System.Drawing.Size(867, 353);
            this.logTB.TabIndex = 0;
            this.logTB.Text = "";
            // 
            // ConnectionStatus
            // 
            this.ConnectionStatus.AutoSize = true;
            this.ConnectionStatus.BackColor = System.Drawing.Color.Red;
            this.ConnectionStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConnectionStatus.Location = new System.Drawing.Point(263, 12);
            this.ConnectionStatus.Name = "ConnectionStatus";
            this.ConnectionStatus.Size = new System.Drawing.Size(119, 20);
            this.ConnectionStatus.TabIndex = 5;
            this.ConnectionStatus.Text = "Disconnected";
            // 
            // ConnectionTransmitter
            // 
            this.ConnectionTransmitter.Interval = 3000;
            this.ConnectionTransmitter.Tick += new System.EventHandler(this.TransmitCommands);
            // 
            // setPW
            // 
            this.setPW.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setPW.Location = new System.Drawing.Point(225, 175);
            this.setPW.Name = "setPW";
            this.setPW.Size = new System.Drawing.Size(141, 23);
            this.setPW.TabIndex = 5;
            this.setPW.Text = "Set new Password";
            this.setPW.UseVisualStyleBackColor = true;
            this.setPW.Click += new System.EventHandler(this.setPW_Click);
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(33, 178);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(159, 20);
            this.password.TabIndex = 6;
            this.password.TextChanged += new System.EventHandler(this.password_TextChanged);
            // 
            // Terminal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 460);
            this.Controls.Add(this.ConnectionStatus);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.IPField);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.connection);
            this.MinimumSize = new System.Drawing.Size(483, 310);
            this.Name = "Terminal";
            this.ShowIcon = false;
            this.Text = "Terminal";
            this.tabControl1.ResumeLayout(false);
            this.SystemControls.ResumeLayout(false);
            this.SystemControls.PerformLayout();
            this.Resources.ResumeLayout(false);
            this.Resources.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EspressoBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MoneyBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SugarBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MilkBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CoffeeBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Statistics.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartBuyed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartAvailable)).EndInit();
            this.Log.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox IPField;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage SystemControls;
        private System.Windows.Forms.TabPage Resources;
        private System.Windows.Forms.TabPage Statistics;
        private System.Windows.Forms.Button shutBT;
        private System.Windows.Forms.Button upBT;
        private System.Windows.Forms.Button restBT;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox CoffeeBar;
        private System.Windows.Forms.PictureBox MoneyBar;
        private System.Windows.Forms.PictureBox SugarBar;
        private System.Windows.Forms.PictureBox MilkBar;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label moneyPer;
        private System.Windows.Forms.Label sugarPer;
        private System.Windows.Forms.Label milkPer;
        private System.Windows.Forms.Label coffeePer;
        private System.Windows.Forms.TextBox MoneyField;
        private System.Windows.Forms.TextBox SugarField;
        private System.Windows.Forms.TextBox MilkField;
        private System.Windows.Forms.TextBox CoffeeField;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label ConnectionStatus;
        private System.Windows.Forms.Timer ConnectionTransmitter;
        private System.Windows.Forms.TabPage Log;
        private System.Windows.Forms.RichTextBox logTB;
        private System.Windows.Forms.TextBox EspressoField;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label espressoPer;
        private System.Windows.Forms.PictureBox EspressoBar;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Button delBT;
        private System.Windows.Forms.Button ReqLogBT;
        private System.Windows.Forms.Button RequestStats;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartBuyed;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAvailable;
        private System.Windows.Forms.Button setPW;
        private System.Windows.Forms.TextBox password;
    }
}

