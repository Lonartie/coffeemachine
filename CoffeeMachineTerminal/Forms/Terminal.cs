﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using CoffeeCore;

namespace CoffeeMachineTerminal
{
    public partial class Terminal : Form
    {
        Client client;

        bool connected = false;

        private DateTime? startRec = null;

        delegate void SettingsSetterDelegate(string mes);

        private SettingsSetterDelegate CommandSetterDelegate;

        string dir = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;

        public Terminal()
        {
            InitializeComponent();
            CommandSetterDelegate = ProcessCommands;
            Logging.AddListener(LogCase.Error, (mes) => { MessageBox.Show(mes, "Error"); return true; });
            try
            {
                using (var web = new WebClient())
                web.DownloadFile("http://lonartie.ddns.net:8080/job/Coffee-Machine%20(Windows)%20(x64)/lastBuild/artifact/output/version.txt",
                    dir + "/ServerVersion.txt");
                StreamReader thisVersion = new StreamReader(dir + "/version.txt");
                StreamReader serverVersion = new StreamReader(dir + "/ServerVersion.txt");
                string thisVersionString = thisVersion.ReadToEnd().Split("\n"[0])[0];
                string serverVersionString = serverVersion.ReadToEnd().Split("\n"[0])[0];
                thisVersion.Close();
                serverVersion.Close();
                if (int.Parse(thisVersionString) != int.Parse(serverVersionString))
                    if (MessageBox.Show(
                            $"New version available!\nYour version: {thisVersionString}\nServer version: {serverVersionString}\nDo you want to update now?",
                            @"Update available!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        ProcessStartInfo info = new ProcessStartInfo("cmd", "/c \"ProcessHelper.exe /u & timeout 5 & taskkill /im CoffeeMachineUI.exe & start CoffeeMachineTerminal.exe\"");
                        Process.Start(info);
                    }

                Text = $@"Terminal version: {thisVersionString}";
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void TransmitCommands(object sender, EventArgs e)
        {
            if (!client.IsConnected())
            {
                ReconnectBehaviour();
                return;
            }

            startRec = null;
            client.OnCommandReceived = (a) => this.Invoke(CommandSetterDelegate, a);
            //client?.SendCommand("GET_LOG");
            //Thread.Sleep(100);
            client?.SendCommand("GET_RES");
        }

        private void ReconnectBehaviour()
        {
            if (startRec == null) startRec = DateTime.Now;
            ConnectionStatus.Text = "Reconnecting";
            ConnectionStatus.BackColor = Color.Yellow;
            client = new Client(IPField.Text);
            connected = Logging.Try(() =>
            {
                client.ConnectToServer();
            }, "Could not connect to server...", LogCase.Info);
            if (!connected)
            {
                if (DateTime.Now - startRec > new TimeSpan(0, 0, 5))
                {
                    ConnectionStatus.Text = "Disconnected";
                    ConnectionStatus.BackColor = Color.Red;
                    connection.Enabled = true;
                    startRec = null;
                    ConnectionTransmitter.Stop();
                }
            }
            else
            {
                ConnectionStatus.Text = "Connected";
                ConnectionStatus.BackColor = Color.Green;
            }
        }

        private void ProcessCommands(string command)
        {
            if (command == "OK")
            {
                MessageBox.Show("Password has been changed!");
            }
            else if (command.Contains("|LOG|"))
            {
                command = command.Remove(0, 5);
                logTB.Text = command;
            }
            else if (command.Contains("|RES|"))
            {
                command = command.Remove(0, 5);
                var storage = CoffeeMachineController.LoadFromString(command);
                ShowResources(storage);
            }
            else if (command.Contains("|STA|"))
            {
                command = command.Remove(0, 5);
                var stats = CoffeeCore.Statistics.FromString(command);
                ShowStats(stats);
            }
            else if (command.Contains("|VER|"))
            {
                command = command.Remove(0, 5);
                try
                {
                    using (var reader = new StreamReader(dir + "/ServerVersion.txt"))
                    {
                        string current = reader.ReadToEnd().Split("\n"[0])[0];
                        if (int.Parse(command) != int.Parse(current))
                            if (MessageBox.Show(
                                    $"CoffeeMachine needs an update!\nCoffee machine remote version: {command}\nCurrent version: {current}\nDo you want to update now?",
                                    "Remote update available", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                UpdateRemote(null, null);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void ShowStats(CoffeeCore.Statistics stats)
        {
            chartAvailable.Series.Clear();
            chartBuyed.Series.Clear();

            foreach (var name in stats.names)
            {
                Chart currentChart = null;
                int index = stats.names.IndexOf(name);
                if (name.Contains("Available"))
                    currentChart = chartAvailable;
                else
                    currentChart = chartBuyed;
                currentChart.Series.Add(name);
                Series currentSeries = currentChart.Series[name];
                currentSeries.BorderWidth = 5;
                if (name.Contains("Money"))
                    currentSeries.ChartArea = "money";
                currentSeries.ChartType = SeriesChartType.Line;

                foreach (var date in stats.dates[index])
                {
                    int dateIndex = stats.dates[index].IndexOf(date);
                    float value = stats.values[index][dateIndex];
                    currentSeries.Points.AddXY(date, value);
                }
            }
            
            chartAvailable.ChartAreas[0].RecalculateAxesScale();
            chartAvailable.ChartAreas[1].RecalculateAxesScale();
            chartBuyed.ChartAreas[0].RecalculateAxesScale();
        }

        private void ShowResources(CoffeeMachineStorage res)
        {
            var availableResources = res.AvailableResources;
            var normal = res.NormalResources;
            UpdateValues(0, availableResources.coffee, normal.coffee);
            UpdateValues(1, availableResources.espresso, normal.espresso);
            UpdateValues(2, availableResources.milk, normal.milk);
            UpdateValues(3, availableResources.sugar, normal.sugar);
            UpdateValues(4, availableResources.money, normal.money);
        }

        private void UpdateValues(int ID, float value, float max)
        {
            PictureBox bar = null;
            Label barVal = null;
            TextBox barRawVal = null;

            if (ID == 0)
            {
                bar = CoffeeBar;
                barVal = coffeePer;
                barRawVal = CoffeeField;
            }
            else if (ID == 1)
            {
                bar = EspressoBar;
                barVal = espressoPer;
                barRawVal = EspressoField;
            }
            else if (ID == 2)
            {
                bar = MilkBar;
                barVal = milkPer;
                barRawVal = MilkField;
            }
            else if (ID == 3)
            {
                bar = SugarBar;
                barVal = sugarPer;
                barRawVal = SugarField;
            }
            else if (ID == 4)
            {
                bar = MoneyBar;
                barVal = moneyPer;
                barRawVal = MoneyField;
            }

            float percentage = value / max;
            bar.Size = new Size(bar.Width, (int)(percentage * pictureBox1.Height));
            bar.Location = new Point(bar.Location.X, pictureBox1.Location.Y + (pictureBox1.Height - bar.Height));
            barVal.Text = ((int) (percentage * 100f)).ToString() + "%";
            barRawVal.Text = (value).ToString();
            Application.DoEvents();
        }

        private void connection_Click(object sender, EventArgs e)
        {
            client = new Client(ip: IPField.Text);
            connected = Logging.Try(() =>
            {
                client.ConnectToServer();
            }, "Could not connect to server...");

            if (!connected) return;

            Thread.Sleep(100);
            client?.SendCommand("GET_VER");
            Thread.Sleep(100);

            ConnectionStatus.Text = "Connected";
            ConnectionStatus.BackColor = Color.Green;

            connection.Enabled = false;

            ConnectionTransmitter.Start();
        }

        private void IPField_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                connection_Click(null, null);
        }

        private void Shutdown(object sender, EventArgs e) =>
            client?.SendCommand("SHUTDWN");

        private void Restart(object sender, EventArgs e) =>
            client?.SendCommand("RESTART");

        private void UpdateRemote(object sender, EventArgs e) =>
            client?.SendCommand("UPDATE");

        private void DeleteConfig(object sender, EventArgs e) =>
            client?.SendCommand("DEL_CON");

        private void ReqLogBT_Click(object sender, EventArgs e) =>
            client?.SendCommand("GET_LOG");

        private void RequestStats_Click(object sender, EventArgs e) =>
            client?.SendCommand("GET_STA");

        private void setPW_Click(object sender, EventArgs e) =>
            client?.SendCommand("|PW|" + password.Text);

        private void password_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int pw = int.Parse(password.Text);
            }
            catch (Exception)
            {
                try
                {
                    password.Text = password.Text.Substring(0, password.Text.Length - 1);
                    password.SelectionStart = password.Text.Length;
                    password.SelectionLength = 0;
                }
                catch (Exception) { }
            }
        }
    }
}
