﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using NUnit.Framework;
using CoffeeCore;

namespace CoffeeMachine.Test
{
	[TestFixture]
    public class ServerClientTest
    {
		[Test, Order(1)]
        public void ServerStartsAndClientCanConnect()
        {
            bool connected = false;

            Server server = new Server()
            {
				OnClientConnected = (ip) =>
                {
                    connected = true;
                    Console.WriteLine(ip + " connected");
                }
            };

            Client client = new Client();

            server.StartServer();
			client.ConnectToServer();

			Thread.Sleep(100);

			Assert.IsTrue(connected);

            client.DisconnectFromServer();
            server.StopServer();
        }

        [Test, Order(2)]
        public void ServerAndClientCanCommunicate()
        {
            string inputMessage = "Hello World!";
            string outputMessage = null;
            var server = new Server()
            {OnCommandReceived = (message) => outputMessage = message};
            var client = new Client();
            server.StartServer();
            client.ConnectToServer();
            Thread.Sleep(100);
            client.SendCommand(inputMessage);
            Thread.Sleep(100);

            client.DisconnectFromServer();
            server.StopServer();

            Assert.AreEqual(inputMessage, outputMessage);
        }

        [Test, Order(3)]
        public void GetLogOverRemoteClientWorks()
        {
            string log = "";
            var machine = new CoffeeCore.CoffeeMachine("");
            var client = new Client
            {OnCommandReceived = (mes) => log = mes};
            client.ConnectToServer();
            Thread.Sleep(100);
            client.SendCommand("GET_LOG");
            Thread.Sleep(100);

            client.DisconnectFromServer();
            machine.StopServer();

            Assert.IsFalse(string.IsNullOrEmpty(log));
            Assert.IsTrue(log.Contains("[Warning]"));
            Console.WriteLine(log);
        }

        [Test, Order(4)]
        public void GetStorageObjectOverNetworkWorks()
        {
            string cont = "";
            var machine = new CoffeeCore.CoffeeMachine("");
            var client = new Client
            { OnCommandReceived = (mes) => cont = mes.Remove(0, 5) };
            client.ConnectToServer();
            Thread.Sleep(100);
            client.SendCommand("GET_RES");
            Thread.Sleep(100);
            var serializer = new XmlSerializer(typeof(CoffeeMachineStorage));
            CoffeeMachineStorage storage = null;
            using (var stream = CommonFunctions.GenerateStreamFromString(cont))
                storage = (CoffeeMachineStorage) serializer.Deserialize(stream);
            var res = storage.AvailableResources;

            client.DisconnectFromServer();
            machine.StopServer();

            Assert.AreEqual(1500, res.sugar);
            Assert.AreEqual(1500, res.milk);
            Assert.AreEqual(1500, res.coffee);
            Assert.AreEqual(1500, res.espresso);
            Assert.AreEqual(0, res.money);
            Assert.AreEqual(storage.AvailableOffers[0].name, "Kaffee");
        }
    }
}
