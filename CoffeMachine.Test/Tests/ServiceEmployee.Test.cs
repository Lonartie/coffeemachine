﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

using CoffeeCore;

namespace CoffeeMachine.Test
{
    [TestFixture]
    public class ServiceEmployeeTest
    {
        [Test, Order(1)]
        public void GetResourcesWorks()
        {
            Resources reference = null;
            Resources transmitted = null;

            bool got = false;

            var controller = new CoffeeMachineController("", true);
            controller
                .SelectProductByName("Kaffee")
                .AddExtraByName("Groß")
                .AddExtraByName("Milch")
                .AddExtraByName("Zucker");
            controller.CalculateOffer();
            reference = controller.GetAvailableResources();
            var client = new Client
            { OnCommandReceived = (a) =>
                {
                    got = true;
                    transmitted = CoffeeMachineController.LoadFromString(a.Remove(0, 5)).AvailableResources;
                }
            };

            client.ConnectToServer();
            Thread.Sleep(1000);
            client.SendCommand("GET_RES");
            Thread.Sleep(1000);

            client.DisconnectFromServer();
            controller.StopServer();
            
            Assert.IsTrue(reference == transmitted);
        }

        [Test, Order(2)]
        public void GetLogWorks()
        {
            string log = "";
            var controller = new CoffeeMachineController("", true);
            var client = new Client
                { OnCommandReceived = (a) => log = a.Remove(0,5)};
            client.ConnectToServer();
            Thread.Sleep(100);
            client.SendCommand("GET_LOG");
            Thread.Sleep(100);

            client.DisconnectFromServer();
            controller.StopServer();

            Assert.IsTrue(log != string.Empty);
        }
    }
}
