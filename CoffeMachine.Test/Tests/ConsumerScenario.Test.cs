﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CoffeeCore;
using NUnit.Framework;

namespace CoffeeMachine.Test
{
    [TestFixture]
    public class ConsumerScenarioTest
    {
        public static readonly List<float> CoinList = new List<float>
        {0.1f, 0.2f, 0.5f, 1.0f, 2.0f, 5.0f};

        bool FloatCompare(float a, float b) =>
            (Math.Round(a * 1000) == Math.Round(b * 1000));

        [Test, Order(1)]
        public void ConsumerScenario_Generic()
        {
            //Assert.IsTrue(false);
            // setting seed for equality of test results!
            Random rand = new Random(9812733);
            List<string> offers = null, extras = null;
            Resources resourcesAvailable = null;
            {
                var machine = new CoffeeCore.CoffeeMachine("", false);
                offers = machine.GetAvailableOfferNames();
                resourcesAvailable = machine.GetStorage().AvailableResources;
                extras = machine.GetAvailableExtrasNames();
            }

            int iteration = 0;

            foreach (var offer in offers)
                foreach (var extra1 in extras)
                    foreach (var extra2 in extras)
                    {
                        List<float> money = new List<float>();
                        int coins = rand.Next(1, 3);
                        for (int n = 0; n < coins; n++)
                            money.Add(CoinList[rand.Next(0, CoinList.Count)]);

                        var machine = new CoffeeCore.CoffeeMachine("", false);

                        machine.SelectProduct(offer);
                        machine.AddExtra("Klein");
                        machine.AddExtra(extra1);
                        machine.AddExtra(extra2);

                        var price = machine.GetPriceOfSelectedProduct();
                        var enough = machine.IsEnoughMoney(money);
                        List<float> change = null;
                        var needed = machine.GetNeededResources();

                        if (enough)
                        {
                            change = machine.InsertMoneyForSelectedProductAndGetChange(money);
                            machine.CalculateOffer();
                        }
                        else
                            machine.Storno();

                        Console.WriteLine($"Iteration: [{++iteration}]");
                        Console.Write(Logging.GetCompleteLog());

                        if (enough)
                        {
                            Assert.IsTrue(FloatCompare(money.Sum() - price, change.Sum()));
                            Assert.IsTrue(resourcesAvailable - needed == machine.GetStorage().AvailableResources);
                        }
                        else
                        {
                            Assert.IsTrue(resourcesAvailable == machine.GetAvailableResources());
                            Assert.IsTrue(price > money.Sum());
                        }
                    }
            Assert.AreEqual(offers.Count * extras.Count * extras.Count, iteration);
        }

        [Test, Order(2)]
        public void ConsumerScenario_01()
        {
            var machine = new CoffeeCore.CoffeeMachine("", false);

            machine.SelectProduct("Kaffee");
            machine.AddExtra("Klein");
            machine.AddExtra("Milch");
            machine.AddExtra("Milch");
            machine.AddExtra("Zucker");

            var enough = machine.IsEnoughMoney(new List<float> { 5f });
            var costs = machine.GetPriceOfSelectedProduct();
            var change = machine.InsertMoneyForSelectedProductAndGetChange(new List<float> { 5f });

            machine.CalculateOffer();

            Console.Write(Logging.GetCompleteLog());

            Assert.AreEqual(true, enough);
            Assert.AreEqual(1.6f, costs);
            Assert.AreEqual(new List<float> { 2f, 1f, 0.2f, 0.2f }, change);
        }

        [Test, Order(3)]
        public void ConsumerScenario_02()
        {
            Resources reference = new Resources {money = -.5f};
            Resources actual;

            var machine = new CoffeeCore.CoffeeMachine("", false);

            machine.SelectProduct("Kafffee");
            machine.AddExtra("Klein");

            machine.Storno();

            machine.SelectProduct("Heißes Wasser");
            machine.AddExtra("Klein");

            actual = machine.GetNeededResources();

            machine.CalculateOffer();

            Assert.IsTrue(reference == actual);
        }
    }
}
