﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoffeeCore;
using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace CoffeeMachine.Test
{
    public class CoffeeCoreTest
    {
        public static string Base = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;

        [Test, Order(1)]
        public void MachineCreatesConfigIfDoesntExist()
        {
            string configFile = Base + "/testConfig.xml";

            if (File.Exists(configFile))
                File.Delete(configFile);

            var configCreated = false;
            Logging.AddListener(LogCase.Info, s =>
            {
                if (s.Contains("config") && s.Contains("saved"))
                    configCreated = true;
                return true;
            });

            var machine = new CoffeeCore.CoffeeMachine(configFile, false);

            var fileExists = File.Exists(configFile);

            if (fileExists)
                File.Delete(configFile);

            Assert.IsTrue(configCreated);
            Assert.IsTrue(fileExists);
        }

        [Test, Order(2)]
        public void MachineCanChangeMoney()
        {
            var machine = new CoffeeCore.CoffeeMachine("", false);
            machine.SelectProduct("Kaffee");
            machine.AddExtra("Klein");
            var result1 = machine.InsertMoneyForSelectedProductAndGetChange(new List<float>
            {2f,0.05f,0.05f});

            var result2 = machine.InsertMoneyForSelectedProductAndGetChange(new List<float>
            {2f,2f,2f,0.5f,0.1f});

            machine.AddExtra("Zucker");
            bool isEnough = machine.IsEnoughMoney(new List<float> { 1f, .1f });

            var result3 = machine.InsertMoneyForSelectedProductAndGetChange(new List<float>
            {1f, .5f, .2f });
            
            Assert.AreEqual(new List<float> { 1f }, result1);
            Assert.AreEqual(new List<float> { 5f, 0.5f }, result2);
            Assert.IsFalse(isEnough);
            Assert.AreEqual(new List<float> { 0.5f }, result3);
        }

        [Test, Order(3)]
        public void MachineStopsWhenNotEnoughResources()
        {
            var controller = new CoffeeMachineController("", false);
            ref CoffeeMachineStorage storage = ref controller.GetStorageObject();
            bool canProceed1 = controller.CanProceed();
            storage.AvailableResources = storage.ThresholdFuel - new Resources { coffee = 1, milk = 1, money = 1, sugar = 1 };
            bool canProceed2 = controller.CanProceed();

            Assert.IsTrue(canProceed1);
            Assert.IsFalse(canProceed2);
        }

        [Test, Order(4)]
        public void MachineCalculatesResourcesCorrectly()
        {
            var controller = new CoffeeMachineController("", false);
            ref var storage = ref controller.GetStorageObject();
            controller.SelectProductByName("Kaffee");
            controller.AddExtraByName("Groß");
            var price = controller.GetPriceOfSelectedProduct();

            var coffee = storage.SelectedOffer.resources;
            var big = storage.SelectedExtras[0].resources;
            var before = controller.GetAvailableResources();

            controller.CalculateOffer();

            var after = controller.GetAvailableResources();
            
            Assert.AreEqual(1.6f, price);
            Assert.AreEqual(before.coffee - coffee.coffee * big.multiplier, after.coffee);
            Assert.AreEqual(before.milk - coffee.milk * big.multiplier, after.milk);
            Assert.AreEqual(before.sugar - coffee.sugar * big.multiplier, after.sugar);
            Assert.AreEqual(before.money + price, after.money);
        }

        [Test, Order(5)]
        public void AvailableOffersAndExtrasAreComplete()
        {
            var machine = new CoffeeCore.CoffeeMachine("", false);
            var offers = machine.GetAvailableOfferNames();
            var referenceOffers = new List<string>
            { "Kaffee", "Kakao", "Cappuccino", "Espresso", "Heißes Wasser", "Heiße Milch"};
            var extras = machine.GetAvailableExtrasNames();
            var referenceExtras = new List<string>
            { "Klein", "Mittel", "Groß", "Kanne", "Milch", "Zucker"};

            Assert.AreEqual(referenceOffers, offers);
            Assert.AreEqual(referenceExtras, extras);
        }

        [Test, Order(6)]
        public void ConfigFileWillBeReused()
        {
            var configFile = Base + "ConfigWillBeReused.xml";
            if (File.Exists(configFile))
                File.Delete(configFile);
            Resources reference;
            // Setting resources through buy
            {
                var controller = new CoffeeMachineController(configFile, false);
                controller
                    .SelectProductByName("Kaffee")
                    .AddExtraByName("Groß")
                    .AddExtraByName("Milch")
                    .AddExtraByName("Zucker");
                controller.CalculateOffer();
                reference = controller.GetAvailableResources();
            }
            // Checking resources through new instance
            {
                var controller = new CoffeeMachineController(configFile, false);
                var loadedResources = controller.GetAvailableResources();
                Assert.IsTrue(reference == loadedResources);
            }

            if (File.Exists(configFile))
                File.Delete(configFile);
        }
    }
}