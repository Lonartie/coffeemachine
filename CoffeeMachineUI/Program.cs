﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using CoffeeCore;

namespace CoffeeMachineUI
{
    static class CoffeeMachineMainProgram
    {
        public static readonly string ConfigFile = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/config.xml";

        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            CoffeeMachine machine = new CoffeeMachine(ConfigFile);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ProductSelectionPage(ref machine));

            machine.StopServer();
        }
    }
}