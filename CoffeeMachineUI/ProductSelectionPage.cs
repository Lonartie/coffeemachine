﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoffeeCore;

namespace CoffeeMachineUI
{
    public partial class ProductSelectionPage : Form
    {
        CoffeeMachine machine;
         
        public ProductSelectionPage(ref CoffeeMachine machine)
        {
            this.machine = machine;
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            machine.SelectProduct("Kaffee");
            machine.CalculateOffer();
        }
    }
}
