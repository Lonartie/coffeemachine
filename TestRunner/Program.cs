﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CoffeeMachine.Test;

namespace TestRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceEmployeeTest();
            ServerClientTest();
            ConsumerScenarioTest();
            CoffeeCoreTest();
        }

        static void CoffeeCoreTest()
        {
            var runner = new CoffeeCoreTest();
            runner.AvailableOffersAndExtrasAreComplete();
            runner.ConfigFileWillBeReused();
            runner.MachineCalculatesResourcesCorrectly();
            runner.MachineCanChangeMoney();
            runner.MachineCreatesConfigIfDoesntExist();
            runner.MachineStopsWhenNotEnoughResources();
        }

        static void ConsumerScenarioTest()
        {
            var runner = new ConsumerScenarioTest();
            runner.ConsumerScenario_01();
            runner.ConsumerScenario_02();
            runner.ConsumerScenario_Generic();
        }

        static void ServerClientTest()
        {
            var runner = new ServerClientTest();
            runner.GetLogOverRemoteClientWorks();
            runner.GetStorageObjectOverNetworkWorks();
            runner.ServerAndClientCanCommunicate();
            runner.ServerStartsAndClientCanConnect();
        }

        static void ServiceEmployeeTest()
        {
            var runner = new ServiceEmployeeTest();
            runner.GetLogWorks();
            runner.GetResourcesWorks();
        }
    }
}
