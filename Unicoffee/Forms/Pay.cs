﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;
using Unicoffee.Controllers;
using Unicoffee.Forms;

namespace Unicoffee
{
    public partial class Pay : Form
    {
        private float 
            price = 0, 
            change = 0, 
            input = 0;

        private List<float> 
            coins = new List<float>(),
            changeCoins = new List<float>();

        private List<Control>
            ThisControls = new List<Control>();

        public Pay()
        {
            FormSwitcher.SetupEvent<Pay>(Loaded);
            InitializeComponent();
            Program.SetupForm(this);
            ThisControls.AddRange(moneyPanel.Controls.Cast<Control>());
            SetupEvents();
            Refresh();
        }

        private void back_Click(object sender, EventArgs e)
        {
            Program.Machine.Storno();
            FormSwitcher.Reset();
        }

        private void next_Click(object sender, EventArgs e)
        {
            if (Program.Machine.IsEnoughMoney(coins) || (Price.Text == Input.Text))
            {
                Program.Machine.CalculateOffer();
                FormSwitcher.SetActiveForm<Bearbeitung>();
            }
        }
        
        public void Loaded()
        { 
            price = Program.Machine.GetPriceOfSelectedProduct();
            Price.Text = String.Format("{0, 0:C2}", price);
            coins.Clear();
            changeCoins.Clear();
        }

        private void SetupEvents()
        {
            foreach (var control in ThisControls)
                try
                {
                    float money = float.Parse((string) control.Tag);
                    control.Click += (a, b) => Insert(money);
                } catch (Exception) { }
        }

        private void Insert(float money)
        {
            coins.Add(money);
            input = coins.Sum();
            Input.Text = String.Format("{0, 0:C2}", input);
            if (Program.Machine.IsEnoughMoney(coins) || (Price.Text == Input.Text))
            {
                changeCoins = Program.Machine.InsertMoneyForSelectedProductAndGetChange(coins);
                change = changeCoins.Sum();
                Change.Text = String.Format("{0, 0:C2}", change);
            }
        }
    }
}
