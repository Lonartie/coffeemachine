﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoffeeCore;
using Unicoffee.Controllers;

namespace Unicoffee.Forms
{
    public partial class LogScreen : Form
    {
        public LogScreen()
        {
            FormSwitcher.SetupEvent<LogScreen>(Loaded);
            InitializeComponent();
            Program.SetupForm(this);
            Refresh();
        }

        private void Loaded() =>
            richTextBox1.Text = Logging.GetCompleteLog();

        private void back_Click(object sender, EventArgs e) =>
            FormSwitcher.SetActiveForm<Wartung>();
    }
}
