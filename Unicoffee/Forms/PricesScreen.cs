﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Unicoffee.Controllers;

namespace Unicoffee.Forms
{
    public partial class PricesScreen : Form
    {
        public PricesScreen()
        {
            FormSwitcher.SetupEvent<PricesScreen>(Loaded);
            InitializeComponent();
            Program.SetupForm(this);
            Refresh();

            initButtons();
        }

        private TextBox currentTB = null;

        private void Loaded()
        {
            var offers = Program.Machine.GetStorage().AvailableOffers;

            coffee.Text = offers.First(x => x.name == "Kaffee").price.ToString();
            milk.Text = offers.First(x => x.name == "Heiße Milch").price.ToString();
            water.Text = offers.First(x => x.name == "Heißes Wasser").price.ToString();
            cacao.Text = offers.First(x => x.name == "Kakao").price.ToString();
            espresso.Text = offers.First(x => x.name == "Espresso").price.ToString();
            cappuccino.Text = offers.First(x => x.name == "Cappuccino").price.ToString();
        }

        private void initButtons()
        {
            foreach (var bt in buttons.Controls.Cast<Button>())
            {
                try
                {
                    int num = int.Parse(bt.Tag.ToString());
                    bt.Text = num.ToString();
                    bt.Click += (a, b) => SendNum(num);
                }
                catch (Exception) { }
            }

            foreach (var control in prices.Controls.Cast<Control>())
            {
                if (control.Tag == null)
                    continue;

                if (control.Tag.ToString() == "tb")
                {
                    control.Click += (a, b) => currentTB = (TextBox) control;
                }
            }
        }

        private void SendNum(int num)
        {
            if (currentTB == null)
                return;

            currentTB.Text += num.ToString();
        }

        private void back_Click(object sender, EventArgs e) =>
            FormSwitcher.SetActiveForm<Wartung>();

        private void button12_Click(object sender, EventArgs e)
        {
            if (currentTB == null)
                return;

            try
            {
                currentTB.Text = currentTB.Text.Substring(0, currentTB.Text.Length - 1);
            }
            catch (Exception) { }
        }

        private void button10_Click(object sender, EventArgs e) =>
            currentTB.Text += ",";

        private void save_Click(object sender, EventArgs e)
        {
            try
            {
                float pcoffee = float.Parse(coffee.Text);
                float pmilk = float.Parse(milk.Text);
                float pwater = float.Parse(water.Text);
                float pespresso = float.Parse(espresso.Text);
                float pcappuccino = float.Parse(cappuccino.Text);
                float pcacao = float.Parse(cacao.Text);
                
                Program.Machine.GetController().SetPrice("Kaffee", pcoffee);
                Program.Machine.GetController().SetPrice("Heiße Milch", pmilk);
                Program.Machine.GetController().SetPrice("Heißes Wasser", pwater);
                Program.Machine.GetController().SetPrice("Kakao", pcacao);
                Program.Machine.GetController().SetPrice("Espresso", pespresso);
                Program.Machine.GetController().SetPrice("Cappuccino", pcappuccino);

                Program.Machine.SaveConfig();

                FormSwitcher.SetActiveForm<Wartung>();
            }
            catch (Exception)
            {
                MessageBox.Show("Beim speichern ist etwas schief gelaufen...\r\nBitte überprüfen Sie ihre Angaben");
            }
        }
    }
}
