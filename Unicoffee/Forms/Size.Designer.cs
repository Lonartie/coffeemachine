﻿namespace Unicoffee
{
    partial class Size
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Size));
            this.CupSmall = new System.Windows.Forms.Button();
            this.CupMedium = new System.Windows.Forms.Button();
            this.CupLarge = new System.Windows.Forms.Button();
            this.Pot = new System.Windows.Forms.Button();
            this.Sugar = new System.Windows.Forms.Button();
            this.Milk = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.next = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.MilkAmount = new System.Windows.Forms.Label();
            this.SugarAmount = new System.Windows.Forms.Label();
            this.Product = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.image = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // CupSmall
            // 
            this.CupSmall.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CupSmall.BackColor = System.Drawing.Color.Transparent;
            this.CupSmall.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CupSmall.BackgroundImage")));
            this.CupSmall.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CupSmall.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CupSmall.FlatAppearance.BorderSize = 3;
            this.CupSmall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CupSmall.Location = new System.Drawing.Point(50, 50);
            this.CupSmall.Margin = new System.Windows.Forms.Padding(50);
            this.CupSmall.Name = "CupSmall";
            this.CupSmall.Size = new System.Drawing.Size(86, 98);
            this.CupSmall.TabIndex = 0;
            this.CupSmall.UseVisualStyleBackColor = false;
            this.CupSmall.Click += new System.EventHandler(this.CupSmall_Click);
            // 
            // CupMedium
            // 
            this.CupMedium.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CupMedium.BackColor = System.Drawing.Color.Transparent;
            this.CupMedium.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CupMedium.BackgroundImage")));
            this.CupMedium.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CupMedium.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CupMedium.FlatAppearance.BorderSize = 3;
            this.CupMedium.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CupMedium.Location = new System.Drawing.Point(211, 25);
            this.CupMedium.Margin = new System.Windows.Forms.Padding(25);
            this.CupMedium.Name = "CupMedium";
            this.CupMedium.Size = new System.Drawing.Size(136, 148);
            this.CupMedium.TabIndex = 1;
            this.CupMedium.UseVisualStyleBackColor = false;
            this.CupMedium.Click += new System.EventHandler(this.CupMedium_Click);
            // 
            // CupLarge
            // 
            this.CupLarge.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CupLarge.BackColor = System.Drawing.Color.Transparent;
            this.CupLarge.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CupLarge.BackgroundImage")));
            this.CupLarge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CupLarge.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CupLarge.FlatAppearance.BorderSize = 3;
            this.CupLarge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CupLarge.Location = new System.Drawing.Point(375, 3);
            this.CupLarge.Name = "CupLarge";
            this.CupLarge.Size = new System.Drawing.Size(180, 192);
            this.CupLarge.TabIndex = 2;
            this.CupLarge.UseVisualStyleBackColor = false;
            this.CupLarge.Click += new System.EventHandler(this.CupLarge_Click);
            // 
            // Pot
            // 
            this.Pot.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Pot.BackColor = System.Drawing.Color.Transparent;
            this.Pot.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Pot.BackgroundImage")));
            this.Pot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Pot.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Pot.FlatAppearance.BorderSize = 3;
            this.Pot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Pot.Location = new System.Drawing.Point(561, 3);
            this.Pot.Name = "Pot";
            this.Pot.Size = new System.Drawing.Size(180, 192);
            this.Pot.TabIndex = 3;
            this.Pot.UseVisualStyleBackColor = false;
            this.Pot.Click += new System.EventHandler(this.Pot_Click);
            // 
            // Sugar
            // 
            this.Sugar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Sugar.BackColor = System.Drawing.Color.Transparent;
            this.Sugar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Sugar.BackgroundImage")));
            this.Sugar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Sugar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Sugar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Sugar.Location = new System.Drawing.Point(375, 201);
            this.Sugar.Name = "Sugar";
            this.Sugar.Size = new System.Drawing.Size(180, 192);
            this.Sugar.TabIndex = 4;
            this.Sugar.UseVisualStyleBackColor = false;
            this.Sugar.Click += new System.EventHandler(this.Sugar_Click);
            // 
            // Milk
            // 
            this.Milk.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Milk.BackColor = System.Drawing.Color.Transparent;
            this.Milk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Milk.BackgroundImage")));
            this.Milk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Milk.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Milk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Milk.Location = new System.Drawing.Point(189, 201);
            this.Milk.Name = "Milk";
            this.Milk.Size = new System.Drawing.Size(180, 192);
            this.Milk.TabIndex = 5;
            this.Milk.UseVisualStyleBackColor = false;
            this.Milk.Click += new System.EventHandler(this.Milk_Click);
            // 
            // back
            // 
            this.back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.back.BackColor = System.Drawing.Color.Transparent;
            this.back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.back.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back.ForeColor = System.Drawing.SystemColors.Control;
            this.back.Location = new System.Drawing.Point(12, 414);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(150, 60);
            this.back.TabIndex = 6;
            this.back.Text = "◄ Zurück";
            this.back.UseVisualStyleBackColor = false;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // next
            // 
            this.next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.next.BackColor = System.Drawing.Color.Transparent;
            this.next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.next.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.next.ForeColor = System.Drawing.SystemColors.Control;
            this.next.Location = new System.Drawing.Point(606, 414);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(150, 60);
            this.next.TabIndex = 7;
            this.next.Text = "Weiter ►";
            this.next.UseVisualStyleBackColor = false;
            this.next.Click += new System.EventHandler(this.next_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.MilkAmount, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.CupSmall, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.CupMedium, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.CupLarge, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.Sugar, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.Milk, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Pot, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.SugarAmount, 3, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(744, 396);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // MilkAmount
            // 
            this.MilkAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MilkAmount.AutoSize = true;
            this.MilkAmount.BackColor = System.Drawing.Color.Transparent;
            this.MilkAmount.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.MilkAmount.ForeColor = System.Drawing.SystemColors.Control;
            this.MilkAmount.Location = new System.Drawing.Point(3, 198);
            this.MilkAmount.Name = "MilkAmount";
            this.MilkAmount.Size = new System.Drawing.Size(180, 198);
            this.MilkAmount.TabIndex = 10;
            this.MilkAmount.Text = "20ml\r\nlöschen";
            this.MilkAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.MilkAmount.Click += new System.EventHandler(this.MilkAmount_Click);
            // 
            // SugarAmount
            // 
            this.SugarAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SugarAmount.AutoSize = true;
            this.SugarAmount.BackColor = System.Drawing.Color.Transparent;
            this.SugarAmount.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.SugarAmount.ForeColor = System.Drawing.SystemColors.Control;
            this.SugarAmount.Location = new System.Drawing.Point(561, 198);
            this.SugarAmount.Name = "SugarAmount";
            this.SugarAmount.Size = new System.Drawing.Size(180, 198);
            this.SugarAmount.TabIndex = 11;
            this.SugarAmount.Text = "20g\r\nlöschen";
            this.SugarAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SugarAmount.Click += new System.EventHandler(this.SugarAmount_Click);
            // 
            // Product
            // 
            this.Product.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Product.BackColor = System.Drawing.Color.Transparent;
            this.Product.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Product.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Product.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Product.ForeColor = System.Drawing.SystemColors.Control;
            this.Product.Location = new System.Drawing.Point(3, 3);
            this.Product.Name = "Product";
            this.Product.Size = new System.Drawing.Size(94, 54);
            this.Product.TabIndex = 10;
            this.Product.Text = "Kaffee";
            this.Product.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.image, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.Product, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(285, 414);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(200, 60);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // image
            // 
            this.image.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.image.BackColor = System.Drawing.Color.Transparent;
            this.image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.image.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.image.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.image.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.image.ForeColor = System.Drawing.SystemColors.Control;
            this.image.Location = new System.Drawing.Point(103, 3);
            this.image.Name = "image";
            this.image.Size = new System.Drawing.Size(94, 54);
            this.image.TabIndex = 11;
            this.image.UseVisualStyleBackColor = false;
            // 
            // Size
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(768, 486);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.next);
            this.Controls.Add(this.back);
            this.Name = "Size";
            this.Text = "Size";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CupSmall;
        private System.Windows.Forms.Button CupMedium;
        private System.Windows.Forms.Button CupLarge;
        private System.Windows.Forms.Button Pot;
        private System.Windows.Forms.Button Sugar;
        private System.Windows.Forms.Button Milk;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button next;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label MilkAmount;
        private System.Windows.Forms.Label SugarAmount;
        private System.Windows.Forms.Button Product;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button image;
    }
}