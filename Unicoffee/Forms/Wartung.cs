﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using CoffeeCore;
using Unicoffee.Controllers;
using Unicoffee.Forms;

namespace Unicoffee
{
    public partial class Wartung : Form
    {
        public Wartung()
        {
            FormSwitcher.SetupEvent<Wartung>(Loaded);
            InitializeComponent();
            Program.SetupForm(this);
            Refresh();
        }

        public void Loaded()
        {
            if (!Password.AskFor())
                FormSwitcher.SetActiveForm<Homescreen>();

            LoadFuelStates();
        }

        private void LoadFuelStates()
        {
            var available = Program.Machine.GetStorage().AvailableResources;
            var normal = Program.Machine.GetStorage().NormalResources;

            FuelState_Coffee.Value = Clamp((int)(available.coffee / (float)normal.coffee * 100), 0, 100);
            FuelState_Milk.Value = Clamp((int)(available.milk / (float)normal.milk * 100), 0, 100);
            FuelState_Espresso.Value = Clamp((int)(available.espresso / (float)normal.espresso * 100), 0, 100);
            FuelState_Sugar.Value = Clamp((int)(available.sugar / (float)normal.sugar * 100), 0, 100);
        }

        private int Clamp(int a, int b, int c) =>
            a < b ? b : a > c ? c : a;

        private void back_Click(object sender, EventArgs e) =>
            FormSwitcher.SetActiveForm<Standby>();

        private void Refill_Click(object sender, EventArgs e)
        {
            var storage = Program.Machine.GetStorage();
            storage.AvailableResources.espresso = storage.NormalResources.espresso;
            storage.AvailableResources.coffee = storage.NormalResources.coffee;
            storage.AvailableResources.milk = storage.NormalResources.milk;
            storage.AvailableResources.sugar = storage.NormalResources.sugar;
            storage.AvailableResources.money = 0;
            LoadFuelStates();
            Program.Machine.SaveConfig();
        }

        private void DeleteConfig_Click(object sender, EventArgs e) =>
            File.Delete(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/config.xml");

        private void Shutdown_Click(object sender, EventArgs e) =>
            Process.GetCurrentProcess().Kill();

        private void Update_Click(object sender, EventArgs e)
        {
            var info = new ProcessStartInfo(
                AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/ProcessHelper.exe",
                "/u");
            Process.Start(info);
        }

        private void button8_Click(object sender, EventArgs e) =>
            MessageBox.Show("Der Kaffeesatz wurde gewechselt, bitte den Müllbeutel entleeren");

        private void button9_Click(object sender, EventArgs e) =>
            MessageBox.Show("Die Maschine wurde gespült!");

        private void button6_Click(object sender, EventArgs e) =>
            MessageBox.Show("Die Maschine wurde vollständig gereinigt!");

        private void button5_Click(object sender, EventArgs e) =>
            FormSwitcher.SetActiveForm<LogScreen>();

        private void button7_Click(object sender, EventArgs e) =>
            FormSwitcher.SetActiveForm<PricesScreen>();

        private void Test_Click(object sender, EventArgs e)
        {
            Program.Machine.StopServer();

            var info = new ProcessStartInfo(
                AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/TestRunner.exe");
            info.RedirectStandardOutput = true;
            info.RedirectStandardError = true;
            info.CreateNoWindow = true;
            info.UseShellExecute = false;
            var process = new Process();
            process.StartInfo = info;
            string error = "", output = "";
            process.ErrorDataReceived += (a, b) =>
            {
                if (!string.IsNullOrEmpty(b.Data))
                    error += b.Data + "\r\n";
            };

            process.OutputDataReceived += (a, b) =>
            {
                if (!string.IsNullOrEmpty(b.Data))
                    output += b.Data + "\r\n";
            };
            process.Exited += (a, b) => MessageBox.Show("test runner done!");
            process.Start();
            process.BeginErrorReadLine();
            process.BeginOutputReadLine();

            DateTime start = DateTime.Now;
            while (!process.HasExited)
            {
                if (!process.Responding)
                    process.Kill();

                if (DateTime.Now - start > TimeSpan.FromMinutes(1))
                    process.Kill();

                Thread.Sleep(1);
            }

            if (!string.IsNullOrEmpty(error))
                MessageBox.Show(error);
            else
                MessageBox.Show("Error 404: Error not found\r\nSystem tests successful");


            StreamWriter writer =
                new StreamWriter(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/TestRunnerLog.txt");
            writer.Write(output);
            writer.Flush();
            writer.Close();

            Program.Machine.StartServer();
        }
    }
}
