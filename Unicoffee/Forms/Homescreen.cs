﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Unicoffee.Controllers;

namespace Unicoffee
{
    public partial class Homescreen : Form
    {
        public Homescreen()
        {
            FormSwitcher.SetupEvent<Homescreen>(Loaded);
            InitializeComponent();
            Program.SetupForm(this);
            Refresh();
        }

        public void Loaded()
        {
            var available = Program.Machine.GetStorage().AvailableResources;
            var normal = Program.Machine.GetStorage().NormalResources;

            FuelState_Coffee.Value = Clamp((int)(available.coffee / (float) normal.coffee * 100), 0, 100);
            FuelState_Milk.Value = Clamp((int)(available.milk / (float) normal.milk * 100),0,100);
            FuelState_Espresso.Value = Clamp((int)(available.espresso / (float) normal.espresso * 100), 0, 100);
            FuelState_Sugar.Value = Clamp((int)(available.sugar / (float) normal.sugar * 100), 0, 100);
        }

        private int Clamp(int a, int b, int c) =>
            a < b ? b : a > c ? c : a;

        private void Button_Coffee_Click(object sender, EventArgs e) =>
            FormSwitcher.SelectAndSwitch<Size>("Kaffee");

        private void Button_Cappuccino_Click(object sender, EventArgs e) =>
            FormSwitcher.SelectAndSwitch<Size>("Cappuccino");

        private void Button_Espresso_Click(object sender, EventArgs e) =>
            FormSwitcher.SelectAndSwitch<Size>("Espresso");

        private void Button_Cocoa_Click(object sender, EventArgs e) =>
            FormSwitcher.SelectAndSwitch<Size>("Kakao");

        private void Button_HotMilk_Click(object sender, EventArgs e) =>
            FormSwitcher.SelectAndSwitch<Size>("Heiße Milch");

        private void Button_HotWater_Click(object sender, EventArgs e) =>
            FormSwitcher.SelectAndSwitch<Size>("Heißes Wasser");

        private void Button_Service_Click(object sender, EventArgs e) =>
            FormSwitcher.SetActiveForm<Wartung>();
    }
}
