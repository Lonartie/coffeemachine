﻿namespace Unicoffee
{
    partial class Homescreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Homescreen));
            this.Button_Coffee = new System.Windows.Forms.Button();
            this.Button_Cappuccino = new System.Windows.Forms.Button();
            this.Button_Espresso = new System.Windows.Forms.Button();
            this.Button_Cocoa = new System.Windows.Forms.Button();
            this.Button_HotMilk = new System.Windows.Forms.Button();
            this.Button_HotWater = new System.Windows.Forms.Button();
            this.FuelState_Coffee = new System.Windows.Forms.ProgressBar();
            this.FuelState_Espresso = new System.Windows.Forms.ProgressBar();
            this.FuelState_Milk = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Button_Service = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.FuelState_Sugar = new System.Windows.Forms.ProgressBar();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Button_Coffee
            // 
            this.Button_Coffee.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Coffee.BackColor = System.Drawing.Color.Transparent;
            this.Button_Coffee.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button_Coffee.BackgroundImage")));
            this.Button_Coffee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Coffee.FlatAppearance.BorderSize = 0;
            this.Button_Coffee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_Coffee.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Coffee.ForeColor = System.Drawing.SystemColors.Window;
            this.Button_Coffee.Location = new System.Drawing.Point(6, 6);
            this.Button_Coffee.Margin = new System.Windows.Forms.Padding(6);
            this.Button_Coffee.Name = "Button_Coffee";
            this.Button_Coffee.Size = new System.Drawing.Size(122, 104);
            this.Button_Coffee.TabIndex = 0;
            this.Button_Coffee.Text = "Kaffee";
            this.Button_Coffee.UseVisualStyleBackColor = false;
            this.Button_Coffee.Click += new System.EventHandler(this.Button_Coffee_Click);
            // 
            // Button_Cappuccino
            // 
            this.Button_Cappuccino.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Cappuccino.BackColor = System.Drawing.Color.Transparent;
            this.Button_Cappuccino.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button_Cappuccino.BackgroundImage")));
            this.Button_Cappuccino.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Cappuccino.FlatAppearance.BorderSize = 0;
            this.Button_Cappuccino.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_Cappuccino.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Cappuccino.ForeColor = System.Drawing.SystemColors.Window;
            this.Button_Cappuccino.Location = new System.Drawing.Point(140, 6);
            this.Button_Cappuccino.Margin = new System.Windows.Forms.Padding(6);
            this.Button_Cappuccino.Name = "Button_Cappuccino";
            this.Button_Cappuccino.Size = new System.Drawing.Size(123, 104);
            this.Button_Cappuccino.TabIndex = 1;
            this.Button_Cappuccino.Text = "Cappuccino";
            this.Button_Cappuccino.UseVisualStyleBackColor = false;
            this.Button_Cappuccino.Click += new System.EventHandler(this.Button_Cappuccino_Click);
            // 
            // Button_Espresso
            // 
            this.Button_Espresso.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Espresso.BackColor = System.Drawing.Color.Transparent;
            this.Button_Espresso.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button_Espresso.BackgroundImage")));
            this.Button_Espresso.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Espresso.FlatAppearance.BorderSize = 0;
            this.Button_Espresso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_Espresso.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Espresso.ForeColor = System.Drawing.SystemColors.Window;
            this.Button_Espresso.Location = new System.Drawing.Point(275, 6);
            this.Button_Espresso.Margin = new System.Windows.Forms.Padding(6);
            this.Button_Espresso.Name = "Button_Espresso";
            this.Button_Espresso.Size = new System.Drawing.Size(124, 104);
            this.Button_Espresso.TabIndex = 2;
            this.Button_Espresso.Text = "Espresso";
            this.Button_Espresso.UseVisualStyleBackColor = false;
            this.Button_Espresso.Click += new System.EventHandler(this.Button_Espresso_Click);
            // 
            // Button_Cocoa
            // 
            this.Button_Cocoa.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Cocoa.BackColor = System.Drawing.Color.Transparent;
            this.Button_Cocoa.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button_Cocoa.BackgroundImage")));
            this.Button_Cocoa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Cocoa.FlatAppearance.BorderSize = 0;
            this.Button_Cocoa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_Cocoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Cocoa.ForeColor = System.Drawing.SystemColors.Window;
            this.Button_Cocoa.Location = new System.Drawing.Point(6, 122);
            this.Button_Cocoa.Margin = new System.Windows.Forms.Padding(6);
            this.Button_Cocoa.Name = "Button_Cocoa";
            this.Button_Cocoa.Size = new System.Drawing.Size(122, 105);
            this.Button_Cocoa.TabIndex = 3;
            this.Button_Cocoa.Text = "Kakao";
            this.Button_Cocoa.UseVisualStyleBackColor = false;
            this.Button_Cocoa.Click += new System.EventHandler(this.Button_Cocoa_Click);
            // 
            // Button_HotMilk
            // 
            this.Button_HotMilk.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_HotMilk.BackColor = System.Drawing.Color.Transparent;
            this.Button_HotMilk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button_HotMilk.BackgroundImage")));
            this.Button_HotMilk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_HotMilk.FlatAppearance.BorderSize = 0;
            this.Button_HotMilk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_HotMilk.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_HotMilk.ForeColor = System.Drawing.SystemColors.Window;
            this.Button_HotMilk.Location = new System.Drawing.Point(140, 122);
            this.Button_HotMilk.Margin = new System.Windows.Forms.Padding(6);
            this.Button_HotMilk.Name = "Button_HotMilk";
            this.Button_HotMilk.Size = new System.Drawing.Size(123, 105);
            this.Button_HotMilk.TabIndex = 4;
            this.Button_HotMilk.Text = "Heiße Milch";
            this.Button_HotMilk.UseVisualStyleBackColor = false;
            this.Button_HotMilk.Click += new System.EventHandler(this.Button_HotMilk_Click);
            // 
            // Button_HotWater
            // 
            this.Button_HotWater.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_HotWater.BackColor = System.Drawing.Color.Transparent;
            this.Button_HotWater.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button_HotWater.BackgroundImage")));
            this.Button_HotWater.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_HotWater.FlatAppearance.BorderSize = 0;
            this.Button_HotWater.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_HotWater.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_HotWater.ForeColor = System.Drawing.SystemColors.Window;
            this.Button_HotWater.Location = new System.Drawing.Point(275, 122);
            this.Button_HotWater.Margin = new System.Windows.Forms.Padding(6);
            this.Button_HotWater.Name = "Button_HotWater";
            this.Button_HotWater.Size = new System.Drawing.Size(124, 105);
            this.Button_HotWater.TabIndex = 5;
            this.Button_HotWater.Text = "Heißes Wasser";
            this.Button_HotWater.UseVisualStyleBackColor = false;
            this.Button_HotWater.Click += new System.EventHandler(this.Button_HotWater_Click);
            // 
            // FuelState_Coffee
            // 
            this.FuelState_Coffee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FuelState_Coffee.Location = new System.Drawing.Point(946, 22);
            this.FuelState_Coffee.Margin = new System.Windows.Forms.Padding(6);
            this.FuelState_Coffee.Name = "FuelState_Coffee";
            this.FuelState_Coffee.Size = new System.Drawing.Size(183, 42);
            this.FuelState_Coffee.TabIndex = 7;
            // 
            // FuelState_Espresso
            // 
            this.FuelState_Espresso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FuelState_Espresso.Location = new System.Drawing.Point(946, 76);
            this.FuelState_Espresso.Margin = new System.Windows.Forms.Padding(6);
            this.FuelState_Espresso.Name = "FuelState_Espresso";
            this.FuelState_Espresso.Size = new System.Drawing.Size(183, 42);
            this.FuelState_Espresso.TabIndex = 8;
            // 
            // FuelState_Milk
            // 
            this.FuelState_Milk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FuelState_Milk.Location = new System.Drawing.Point(946, 129);
            this.FuelState_Milk.Margin = new System.Windows.Forms.Padding(6);
            this.FuelState_Milk.Name = "FuelState_Milk";
            this.FuelState_Milk.Size = new System.Drawing.Size(183, 42);
            this.FuelState_Milk.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label1.Location = new System.Drawing.Point(764, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(288, 42);
            this.label1.TabIndex = 10;
            this.label1.Text = "Füllstand Kaffee";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label2.Location = new System.Drawing.Point(742, 87);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(337, 42);
            this.label2.TabIndex = 11;
            this.label2.Text = "Füllstand Espresso";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label3.Location = new System.Drawing.Point(775, 140);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(267, 42);
            this.label3.TabIndex = 12;
            this.label3.Text = "Füllstand Milch";
            // 
            // Button_Service
            // 
            this.Button_Service.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Button_Service.BackColor = System.Drawing.Color.White;
            this.Button_Service.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button_Service.BackgroundImage")));
            this.Button_Service.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Service.FlatAppearance.BorderSize = 0;
            this.Button_Service.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_Service.Font = new System.Drawing.Font("Segoe UI Black", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Service.ForeColor = System.Drawing.SystemColors.Window;
            this.Button_Service.Location = new System.Drawing.Point(22, 598);
            this.Button_Service.Margin = new System.Windows.Forms.Padding(6);
            this.Button_Service.Name = "Button_Service";
            this.Button_Service.Size = new System.Drawing.Size(73, 74);
            this.Button_Service.TabIndex = 13;
            this.Button_Service.UseVisualStyleBackColor = false;
            this.Button_Service.Click += new System.EventHandler(this.Button_Service_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label4.Location = new System.Drawing.Point(757, 194);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(295, 42);
            this.label4.TabIndex = 14;
            this.label4.Text = "Füllstand Zucker";
            // 
            // FuelState_Sugar
            // 
            this.FuelState_Sugar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FuelState_Sugar.Location = new System.Drawing.Point(946, 183);
            this.FuelState_Sugar.Margin = new System.Windows.Forms.Padding(6);
            this.FuelState_Sugar.Name = "FuelState_Sugar";
            this.FuelState_Sugar.Size = new System.Drawing.Size(183, 42);
            this.FuelState_Sugar.TabIndex = 15;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.Controls.Add(this.Button_Coffee, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Button_Cappuccino, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Button_Espresso, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.Button_Cocoa, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Button_HotMilk, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Button_HotWater, 2, 1);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(371, 259);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(405, 233);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // Homescreen
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1151, 694);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.FuelState_Sugar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Button_Service);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FuelState_Milk);
            this.Controls.Add(this.FuelState_Espresso);
            this.Controls.Add(this.FuelState_Coffee);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Homescreen";
            this.Text = "Homescreen";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button Button_Coffee;
        public System.Windows.Forms.Button Button_Cappuccino;
        public System.Windows.Forms.Button Button_Espresso;
        public System.Windows.Forms.Button Button_Cocoa;
        public System.Windows.Forms.Button Button_HotMilk;
        public System.Windows.Forms.Button Button_HotWater;
        private System.Windows.Forms.ProgressBar FuelState_Coffee;
        private System.Windows.Forms.ProgressBar FuelState_Espresso;
        private System.Windows.Forms.ProgressBar FuelState_Milk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Button_Service;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ProgressBar FuelState_Sugar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}