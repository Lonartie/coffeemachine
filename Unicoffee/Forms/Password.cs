﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoffeeCore;
using Unicoffee.Controllers;

namespace Unicoffee.Forms
{
    public partial class Password : Form
    {
        public static bool isCorrect = false;

        public static bool AskFor()
        {
            isCorrect = false;
            (new Password()).ShowDialog();
            return isCorrect;
        }

        private string pwString = "";

        public Password()
        {
            InitializeComponent();
            LoadPW();
            init();
        }

        private void LoadPW()
        {
            pwString = Program.Machine.GetStorage().password;
        }

        private void init()
        {
            foreach (var bt in buttons.Controls.Cast<Button>())
            {
                try
                {
                    int num = int.Parse(bt.Tag.ToString());
                    bt.Text = num.ToString();
                    bt.Click += (a, b) => SendNum(num);
                }
                catch (Exception) { }
            }
        }

        private void CheckPW(string pw)
        {
            if (pw.Equals(pwString))
            {
                isCorrect = true;
                Close();
            }
        }

        private void SendNum(int num)
        {
            pw.Text += num;
            CheckPW(pw.Text);
        }

        private void back(object sender, EventArgs e) =>
            Close();

        private void deleteLast(object sender, EventArgs e) =>
            Logging.Try(() => { pw.Text = pw.Text.Substring(0, pw.Text.Length - 1); }, "");
    }
}
