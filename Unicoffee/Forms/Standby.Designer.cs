﻿namespace Unicoffee
{
    partial class Standby
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Standby));
            this.Button_PressToStart = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Button_PressToStart
            // 
            this.Button_PressToStart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_PressToStart.BackColor = System.Drawing.Color.Transparent;
            this.Button_PressToStart.FlatAppearance.BorderSize = 0;
            this.Button_PressToStart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Button_PressToStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Button_PressToStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_PressToStart.Font = new System.Drawing.Font("Segoe UI Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_PressToStart.ForeColor = System.Drawing.Color.White;
            this.Button_PressToStart.Location = new System.Drawing.Point(22, 22);
            this.Button_PressToStart.Margin = new System.Windows.Forms.Padding(6);
            this.Button_PressToStart.Name = "Button_PressToStart";
            this.Button_PressToStart.Size = new System.Drawing.Size(679, 434);
            this.Button_PressToStart.TabIndex = 0;
            this.Button_PressToStart.TabStop = false;
            this.Button_PressToStart.Text = "Zum Starten berühren";
            this.Button_PressToStart.UseVisualStyleBackColor = false;
            this.Button_PressToStart.Click += new System.EventHandler(this.Button_PressToStart_Click);
            // 
            // Standby
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(723, 478);
            this.Controls.Add(this.Button_PressToStart);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Standby";
            this.Text = "Standby";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Button_PressToStart;
    }
}

