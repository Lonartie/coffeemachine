﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Unicoffee.Controllers;

namespace Unicoffee
{
    public partial class Size : Form
    {
        public Size()
        {
            FormSwitcher.SetupEvent<Size>(Loaded);
            InitializeComponent();
            Program.SetupForm(this);
            Refresh();
        }

        public void Loaded()
        {
            Product.Text = Program.Machine.GetSelectedProductName();
            UpdateImage(Program.Machine.GetSelectedProductName());
            UpdateAmounts();
        }

        private void UpdateImage(string product)
        {
            Image image = null;

            if (product == "Kaffee") image = FormSwitcher.GetInstance<Homescreen>().Button_Coffee.BackgroundImage;
            if (product == "Espresso") image = FormSwitcher.GetInstance<Homescreen>().Button_Espresso.BackgroundImage;
            if (product == "Kakao") image = FormSwitcher.GetInstance<Homescreen>().Button_Cocoa.BackgroundImage;
            if (product == "Heißes Wasser") image = FormSwitcher.GetInstance<Homescreen>().Button_HotWater.BackgroundImage;
            if (product == "Heiße Milch") image = FormSwitcher.GetInstance<Homescreen>().Button_HotMilk.BackgroundImage;
            if (product == "Cappuccino") image = FormSwitcher.GetInstance<Homescreen>().Button_Cappuccino.BackgroundImage;

            this.image.BackgroundImage = image;
        }

        private Button SelectedSize = null;

        private void UpdateAmounts()
        {
            var extras = Program.Machine.GetStorage().SelectedExtras;
            int milkA = extras.Count(x => x.name == "Milch") * 10;
            int sugarA = extras.Count(x => x.name == "Zucker") * 10;

            MilkAmount.Text = $"{milkA}ml\r\nlöschen";
            SugarAmount.Text = $"{sugarA}g\r\nlöschen";

            CupSmall.FlatAppearance.BorderColor = Color.Black;
            CupMedium.FlatAppearance.BorderColor = Color.Black;
            CupLarge.FlatAppearance.BorderColor = Color.Black;
            Pot.FlatAppearance.BorderColor = Color.Black;

            if (SelectedSize != null)
            SelectedSize.FlatAppearance.BorderColor = Color.White;
        }

        private void CheckAmount(string last)
        {
            var needed = Program.Machine.GetNeededResources();
            var storage = Program.Machine.GetStorage().AvailableResources;

            bool proceed = last == "Zucker" ? needed.sugar <= storage.sugar : needed.milk <= storage.milk;

            if (!proceed) 
            {
                Program.Machine.DeleteExtra(last);
                MessageBox.Show("Leider stehen nicht genug Resourcen dafür zur Verfügung..");
            }
        }

        private void CupSmall_Click(object sender, EventArgs e)
        {
            SelectedSize = (Button) sender;
            Program.Machine.ResetSize();
            Program.Machine.AddExtra("Klein");
            UpdateAmounts();
        }

        private void CupMedium_Click(object sender, EventArgs e)
        {
            SelectedSize = (Button)sender;
            Program.Machine.ResetSize();
            Program.Machine.AddExtra("Mittel");
            UpdateAmounts();
        }

        private void CupLarge_Click(object sender, EventArgs e)
        {
            SelectedSize = (Button)sender;
            Program.Machine.ResetSize();
            Program.Machine.AddExtra("Groß");
            UpdateAmounts();
        }

        private void Pot_Click(object sender, EventArgs e)
        {
            SelectedSize = (Button)sender;
            Program.Machine.ResetSize();
            Program.Machine.AddExtra("Kanne");
            UpdateAmounts();
        }

        private void MilkAmount_Click(object sender, EventArgs e)
        {
            Program.Machine.DeleteExtra("Milch");
            UpdateAmounts();
        }

        private void Milk_Click(object sender, EventArgs e)
        {
            Program.Machine.AddExtra("Milch");
            CheckAmount("Milch");
            UpdateAmounts();
        }

        private void Sugar_Click(object sender, EventArgs e)
        {
            Program.Machine.AddExtra("Zucker");
            CheckAmount("Zucker");
            UpdateAmounts();
        }

        private void SugarAmount_Click(object sender, EventArgs e)
        {
            Program.Machine.DeleteExtra("Zucker");
            UpdateAmounts();
        }

        private void back_Click(object sender, EventArgs e)
        {
            Program.Machine.Storno();
            FormSwitcher.Reset();
        }

        private void next_Click(object sender, EventArgs e)
        {
            if (SelectedSize == null)
            {
                MessageBox.Show("Bitte wählen sie eine Größe aus!");
                return;
            }

            FormSwitcher.SetActiveForm<Pay>();
        }
    }
}
