﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Unicoffee.Controllers;

namespace Unicoffee
{
    public partial class Wartung_nötig : Form
    {
        public Wartung_nötig()
        {
            InitializeComponent();
            Program.SetupForm(this);
            Refresh();
        }

        private void back_Click(object sender, EventArgs e) =>
            FormSwitcher.SetActiveForm<Wartung>();
    }
}
