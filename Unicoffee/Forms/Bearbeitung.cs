﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Unicoffee.Controllers;

namespace Unicoffee.Forms
{
    public partial class Bearbeitung : Form
    {
        public Bearbeitung()
        {
            FormSwitcher.SetupEvent<Bearbeitung>(Loaded);
            InitializeComponent();
            Program.SetupForm(this);
            Refresh();
        }

        private void Loaded()
        {
            Timer timer = new Timer {Interval = 5 * 1000 / 100};
            timer.Tick += (a, b) =>
            {
                if (progressBar1.Value == 100)
                {
                    timer.Stop();
                    MessageBox.Show("Bitte entnehmen Sie ihr Getränk!");
                    Program.Machine.Storno();
                    FormSwitcher.Reset();
                    return;
                }
                progressBar1.Value++;
            };
            timer.Start();
        }
    }
}
