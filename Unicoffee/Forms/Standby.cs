﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Unicoffee.Controllers;

namespace Unicoffee
{
    public partial class Standby : Form
    {
        public Standby()
        {
            FormSwitcher.SetupEvent<Standby>(Loaded);
            InitializeComponent();
            Program.SetupForm(this);
            Refresh();
        }

        private void Button_PressToStart_Click(object sender, EventArgs e) =>
            FormSwitcher.SetActiveForm<Homescreen>();

        private void Loaded()
        {
            if (!Program.Machine.CanProceed())
                FormSwitcher.SetActiveForm<Wartung_nötig>();
        }
    }
}
