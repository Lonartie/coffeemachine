﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Unicoffee.Controllers;
using Unicoffee.Forms;

namespace Unicoffee.Container
{
    public partial class ContainerForm : Form
    {
        public static ContainerForm Instance = null;
        
        public ContainerForm()
        {
            InitializeComponent();
            Program.SetupForm(this);
            Instance = this;

            FormSwitcher.InitializeAllForms(
                typeof(Danke),
                typeof(Homescreen),
                typeof(Pay),
                typeof(Size),
                typeof(Standby),
                typeof(Wartung),
                typeof(Wartung_nötig),
                typeof(Bearbeitung),
                typeof(LogScreen),
                typeof(PricesScreen)
            );
        }

        private void ContainerForm_Load(object sender, EventArgs e) =>
            FormSwitcher.SetActiveForm<Standby>();
    }
}
