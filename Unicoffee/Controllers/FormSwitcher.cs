﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Text;
using System.Windows.Forms;
using Unicoffee.Container;

namespace Unicoffee.Controllers
{
    public static class FormSwitcher
    {
        [DllImport("user32.dll")]
        private static extern long LockWindowUpdate(IntPtr Handle);

        private static List<FormControl> activeControls = new List<FormControl>();

        private static Dictionary<Type, Action> startups = new Dictionary<Type, Action>();

        private static FormControl activeControl;
        
        public static void InitializeAllForms(params Type[] types)
        {
            foreach (var control in activeControls)
                control.form.Dispose();
            activeControls.Clear();
            activeControl?.form.Dispose();
            activeControl = null;
            foreach (var tp in types)
            {
                var temp = (Form) Activator.CreateInstance(tp);
                var list = temp.Controls.Cast<Control>().ToList();
                activeControls.Add(new FormControl
                {
                    form = temp,
                    type = tp,
                    controls = list
                });
            }
        }
        
        public static void SetActiveForm<T>() where T : Form
        {
            LockWindowUpdate(ContainerForm.Instance.Handle);
            UnbindOldControls();
            activeControl = activeControls.First(x => x.type == typeof(T));
            ContainerForm.Instance.Controls.AddRange (activeControl.controls.ToArray());
            ContainerForm.Instance.BackgroundImage = activeControl.form.BackgroundImage;
            if (startups.ContainsKey(activeControl.type)) startups[activeControl.type]?.Invoke();
            LockWindowUpdate(IntPtr.Zero);
        }

        private static void UnbindOldControls()
        {
            activeControl?.form.Controls.AddRange(ContainerForm.Instance.Controls.Cast<Control>().ToArray());

            ContainerForm.Instance.Controls.Clear();
        }

        public static void SetupEvent<T>(Action loaded) where T : Form =>
            startups.Add(typeof(T), loaded);

        public static void SelectAndSwitch<T>(string product) where T : Form
        {
            if (!Program.Machine.CanProceed())
            {
                MessageBox.Show("Leider stehen nicht genug Resourcen dafür zur Verfügung..");
                return;
            }

            Program.Machine.SelectProduct(product);
            SetActiveForm<T>();
        }

        public static T GetInstance<T>() where T : Form =>
            (T) activeControls.First(x => x.type == typeof(T)).form;

        public static void Reset()
        {
            List<Type> types = activeControls.Select(x => x.type).ToList();
            startups.Clear();
            InitializeAllForms(types.ToArray());
            ContainerForm.Instance.Controls.Clear();
            SetActiveForm<Standby>();
        }
    }

    public class FormControl
    {
        public Form form;

        public Type type;

        public List<Control> controls;

        public Action startup;
    }
}
