﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using CoffeeCore;
using Unicoffee.Container;
using Unicoffee.Controllers;
using Unicoffee;

namespace Unicoffee
{
    static class Program
    {
        public static int Width
        { get => Screen.PrimaryScreen.Bounds.Width; }
        
        public static int Height
        { get => Screen.PrimaryScreen.Bounds.Height; }

        /// <summary>
        /// static path to the configuration file
        /// </summary>
        public static readonly string ConfigPath =
            AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/config.xml";

        /// <summary>
        /// the coffee machine itself
        /// </summary>
        public static CoffeeMachine Machine;

        public static void SetupForm(Form form)
        {
            form.FormBorderStyle = FormBorderStyle.None;
            //form.TopMost = true;
            form.Bounds = Screen.PrimaryScreen.Bounds;
        }


        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Machine = 
                new CoffeeMachine(ConfigPath);

            Application.Run(new ContainerForm());
        }
    }
}
